<!DOCTYPE html>

<html dir="ltr" lang="en-US">

<head>



	<meta http-equiv="content-type" content="text/html; charset=utf-8" />

	<meta name="author" content="SemiColonWeb" />



	<!-- Stylesheets

	============================================= -->

	 <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Ubuntu:300,300i,400,400i,500,500i,600,600i,700,700i,800,800i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

	<link href="https://fonts.googleapis.com/css?family=Lato:300,400,400i,700|Raleway:300,400,500,600,700|Crete+Round:400i" rel="stylesheet" type="text/css" />

    <link rel="stylesheet" href="{{URL::asset('UI/css/font-awesome.css')}}">

    <link rel="stylesheet" href="{{URL::asset('UI/css/bootstrap.css')}}" type="text/css"/>

    <link rel="stylesheet" href="{{URL::asset('UI/css/style.css')}}" type="text/css"/>

    <link rel="stylesheet" href="{{URL::asset('UI/css/style2.css')}}" type="text/css"/>

    <link rel="stylesheet" href="{{URL::asset('UI/css/dark.css')}}" type="text/css"/>

    <link rel="stylesheet" href="{{URL::asset('UI/css/font-icons.css')}}" type="text/css"/>

    <link rel="stylesheet" href="{{URL::asset('UI/css/animate.css')}}" type="text/css"/>

    <link rel="stylesheet" href="{{URL::asset('UI/css/magnific-popup.css')}}" type="text/css"/>

    <!-- Date & Time Picker CSS -->

    <link rel="stylesheet" href="{{URL::asset('UI/css/datepicker.css')}}" type="text/css"/>

    <!-- Hosting Demo Specific Stylesheet -->

    <link rel="stylesheet" href="{{URL::asset('UI/css/course.css')}}" type="text/css"/>

    <link rel="stylesheet" href="{{URL::asset('UI/css/responsive.css')}}" type="text/css"/>

	<meta name="viewport" content="width=device-width, initial-scale=1" />



	<!-- Document Title

	============================================= -->

	<title>::Welcome to Skillsgroom | Trainer Details::</title>



</head>



<body class="stretched">



	<!-- Document Wrapper

	============================================= -->

	<div id="wrapper" class="clearfix">



		<!-- Header

		============================================= -->

		<header id="header" class="transparent-header dark full-header" data-sticky-class="not-dark">



			<div id="header-wrap">



				<div class="container clearfix">



					<!--<div id="primary-menu-trigger"><i class="icon-reorder"></i></div>-->



					<!-- Logo

					============================================= -->

                    <div id="logo">

                        <a href="/index.php" class="standard-logo" data-dark-logo="{{URL::asset('UI/images/logo-dark.png')}}">

                          <img src="{{URL::asset('UI/images/logo.png')}}" alt="Skillsgroom">

                        </a>

                        <a href="/index.php" class="retina-logo" data-dark-logo="{{URL::asset('UI/images/logo-dark@2x.png')}}">

                          <img src="{{URL::asset('UI/images/logo-dark.png')}}" alt="Skillsgroom">

                        </a>

                      </div>



					<!-- Primary Navigation

					============================================= -->

					<nav id="primary-menu">



						<ul>

							<!--<li><a href="index.html"><div>Home</div></a></li>

							<li><a href="about.html"><div>About Us</div></a></li>

							<li><a href="contact.html"><div>Contact Us</div></a></li>-->



						</ul>



					</nav><!-- #primary-menu end -->



				</div>



			</div>



		</header><!-- #header end -->



		<section id="slider" class="slider-element slider-parallax mobile_forum" style="background: url('{{URL::asset('UI/images/inner_banner.jpg')}}') no-repeat; background-size: cover">



		</section>





		<!-- Content

		============================================= -->

		<div id="content">

<div class="content-wrap py-0">





<div class="section mt-0 mb-0" style="padding:50px 0 0; background: #efefef;">

					<div class="container">

                        <form action="/search_course" method="GET">
                            @csrf
                        <div class="input-group divcenter mb-4">

                              <div class="input-group-prepend">
                                  <div class="input-group-text"><i class="icon-email2"></i></div>
                              </div>
                              <input type="text" id="widget-subscribe-form-email" name="course_name" class="form-control required email" placeholder="Type subject or topic keyword to search">
                              <div class="input-group-append">
                                  <button class="btn btn-success" type="submit">Search</button>
                              </div>

                        </div>
                    </form>

						<div class="section-title">

					  <h2>Online Courses and Events </h2>



					</div>



						<div class="row mt-2">

                            @if(isset($Courses))

                            @foreach($Courses as $Course)

							<!-- Course 1

							============================================= -->

                                <div class="col-md-4 mb-5">

                                    <div class="card course-card hover-effect noborder">

                                        <a href="course_details/{{$Course['id']}}" target="_blank">

                                          @if($Course['course_image'])

                                            <img class="card-img-top" src="https://onlinelms.skillsgroom.com/UI/courses/{{$Course['course_image']}}" alt="Card image cap" />

                                          @else

                                            <img class="card-img-top" src="https://onlinelms.skillsgroom.com/UI/images/1.jpg" alt="Card image cap" />

                                          @endif



                                        </a>

                                        <div class="card-body">

                                            <h4 class="card-title t700 mb-2"><a href="course_details/{{$Course['id']}}" target="_blank">{{$Course['title']}}</h4>

                                            <p class="mb-2 card-title-sub uppercase t400 ls1"><a href="#" class="text-black-50">Category -

                                                @if($Course['category'] == 1)

                                                Tuition & support classes

                                              @elseif($Course['category'] == 2)

                                                Hobbies & Activities

                                              @elseif($Course['category'] == 3)

                                                Technology Trainings

                                              @elseif($Course['category'] == 4)

                                                Certification Courses

                                              @elseif($Course['category'] == 5)

                                                Events & seminars

                                              @elseif($Course['category'] == 6)

                                                Exam and Test

                                              @elseif($Course['category'] == 7)

                                                Job interview preparation

                                              @elseif($Course['category'] == 8)

                                                Mentorship & coaching

                                              @endif

                                            </a></p>



                                            {{-- <p class="card-text text-black-50 mb-1">{!! Str::words($Course['course_description'], 10 ) !!} </p> --}}



                                            <p class="card-text text-black-50 mb-1" style="height: 60px;">{{$Course['who_should_join']}}</p>

                                        </div>

                                        <div class="card-footer py-3 d-flex justify-content-between align-items-center bg-white text-muted">

                                            <div class="badge alert-primary">
                                                @if(isset($Course['course_date']))
                                                    {{date('d M Y', strtotime($Course['course_date']))}}
                                                @else

                                                @endif

                                            </div>

                                            <div class="badge alert-warning">Duration : {{$Course['duration']}}

                                            </div>

                                        </div>

                                    </div>

                                </div>

                            @endforeach



                            @endif



						</div>

					</div>

			</div>



			</div>

		</div><!-- #content end -->



		<!-- Footer

		============================================= -->

		<footer id="footer" class="dark">

    <div class="container">

      <!-- Footer Widgets
              ============================================= -->
      <div class="footer-widgets-wrap clearfix">

        <div class="col_two_third">

          <div class="col_one_third">

            <div class="widget clearfix">

              <!--<h4>Locations</h4>-->
              <div style="background: url('images/world-map.png') no-repeat center center; background-size: 100%;">
                <address>
                  B305, Celebrity Square, Bidaraguppe<br>
                  Sarjapur Attibele Main Road,<br>
                  Bengaluru, Karnataka, India -562107.
                </address>
                <abbr title="Phone Number"><strong>Phone:</strong></abbr> +91 9916669702<br>
                <abbr title="Email Address"><strong>Email:</strong></abbr> <a href="mailto:info@skillsgroom.com">info@skillsgroom.com</a>
                <div class="widget clearfix">
                  <!--<h4>Follow Us</h4>-->
                  <a href="https://www.facebook.com/SkillsGroom" target="_blank" class="social-icon si-dark si-colored si-facebook nobottommargin"
                     style="margin-right: 10px;">
                    <i class="icon-facebook"></i>
                    <i class="icon-facebook"></i>
                  </a>
                  <!--<a href="#" class="social-icon si-dark si-colored si-twitter nobottommargin" style="margin-right: 10px;">
                                          <i class="icon-twitter"></i>
                                          <i class="icon-twitter"></i>
                                      </a>-->
                  <a href="https://www.instagram.com/skills_groom" target="_blank" class="social-icon si-dark si-colored si-instagram nobottommargin"
                     style="margin-right: 10px;">
                    <i class="icon-instagram"></i>
                    <i class="icon-instagram"></i>
                  </a>
                  <a href="https://www.youtube.com/channel/UCwhWbSkbUCn8_vG_06sj8cQ?view_as=subscriber" target="_blank" class="social-icon si-dark si-colored si-youtube nobottommargin"
                     style="margin-right: 10px;">
                    <i class="icon-youtube"></i>
                    <i class="icon-youtube"></i>
                  </a>
                </div>
              </div>

            </div>

          </div>
			<div class="col_one_third">

							<div class="widget widget_links clearfix">

								<ul>
									<li><a href="about.html">About Us</a></li>
									<li><a href="https://skillsgroom.com/sites/hobbyclasses/"><div>Music and Hobby classe</div></a></li>
								<li><a href="https://skillsgroom.com/sites/hobbyclasses/"><div>Chess and Sudoku classes</div></a></li>
								<li><a href="https://skillsgroom.com/sites/technicaltraining/"><div>Technical Trainings</div></a></li>

										</ul>


							</div>

						</div>
			<div class="col_one_third col_last">

							<div class="widget widget_links clearfix">

								<ul>

								<li><a href="coding_kids.html"><div>Coding for Kids</div></a></li>
								<li><a href="tuition.html"><div>Tuition</div></a></li>
								<li><a href="homeschooling.html"><div>Homeschooling</div></a></li>
								<li><a href="#"><div>Softskills and professional skills</div></a></li>
										</ul>


							</div>

						</div>
		  </div>

			<div class="col_one_third col_last">

							<div class="widget widget_links clearfix">

								<ul>
								<li><a href="solutions.html"><div>Solutions for School /Colleges</div></a></li>
										</ul>


							</div>

						</div>




      </div><!-- .footer-widgets-wrap end -->

    </div>

    <!-- Copyrights
          ============================================= -->
    <div id="copyrights">

      <div class="container clearfix">


        &copy; Copyright 2020 All Rights Reserved by <a href="index.php">Skillsgroom</a>
<br>
<div class="copyright-links"><a href="terms.php">Terms of Use</a> / <a href="privacy.php">Privacy Policy</a></div>

      </div>

    </div><!-- #copyrights end -->

  </footer><!-- #footer end -->



	</div><!-- #wrapper end -->



	<!-- Go To Top

	============================================= -->

	<div id="gotoTop" class="icon-angle-up"></div>



	<!-- External JavaScripts

	============================================= -->

    <script data-cfasync="false" src="/cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js">

    </script>

    <script src="{{URL::asset('UI/js/jquery.js')}}">

    </script>

    <script src="{{URL::asset('UI/js/plugins.js')}}">

    </script>



    <script src="{{URL::asset('UI/js/jquery.cslider.js')}}">

    </script>

    <script src="{{URL::asset('UI/js/datepicker.js')}}">

    </script>

    <!-- Footer Scripts

  ============================================= -->

    <script src="{{URL::asset('UI/js/functions.js')}}">

    </script>

    <script src="{{URL::asset('UI/js/custom/class.js')}}">

    </script>







</body>

</html>
