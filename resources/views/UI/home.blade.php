<!DOCTYPE html>



<html dir="ltr" lang="en-US">



  <head>



    <!-- Global site tag (gtag.js) - Google Analytics -->



    <script async src="https://www.googletagmanager.com/gtag/js?id=G-0TRM90RCRT">



    </script>



    <script>



      window.dataLayer = window.dataLayer || [];



      function gtag(){



        dataLayer.push(arguments);



      }



      gtag('js', new Date());



      gtag('config', 'G-0TRM90RCRT');



    </script>



    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>



    <?php
    $GetCourses= json_decode(file_get_contents("https://onlinelms.skillsgroom.com/api/course_list/2"), true);

    // echo json_encode($GetCourses['data']);
    // exit;

    $Courses = $GetCourses['data'];
    ?>

        <meta http-equiv="content-type" content="text/html; charset=utf-8"/>


        <meta name="author" content="Skillsgroom">
        <meta name="description" content="Skillsgroom is India's fastest growing online teaching solutions provider for Sudoku, Chess, Music and Extracurricular Activities. We are making online learning experience more effective and affordable.">
        <meta name="keywords" content="@foreach($Courses as $Title) {{ $Title['title'] }}, @endforeach">



    <meta name="csrf-token" content="{{ csrf_token() }}" />



    <!-- Stylesheets



============================================= -->



    <link rel="icon" href="images/favicon.ico" type="image/x-icon"/>



    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon"/>



    <link



          href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Ubuntu:300,300i,400,400i,500,500i,600,600i,700,700i,800,800i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i"



          rel="stylesheet">



    <link



          href="https://fonts.googleapis.com/css?family=Lato:300,400,400i,700|Raleway:300,400,500,600,700|Crete+Round:400i"



          rel="stylesheet" type="text/css"/>



    <link rel="stylesheet" href="{{URL::asset('UI/css/font-awesome.css')}}">



    <link rel="stylesheet" href="{{URL::asset('UI/css/bootstrap.css')}}" type="text/css"/>



    <link rel="stylesheet" href="{{URL::asset('UI/css/style.css')}}" type="text/css"/>



    <link rel="stylesheet" href="{{URL::asset('UI/css/style2.css')}}" type="text/css"/>



    <link rel="stylesheet" href="{{URL::asset('UI/css/dark.css')}}" type="text/css"/>



    <link rel="stylesheet" href="{{URL::asset('UI/css/font-icons.css')}}" type="text/css"/>



    <link rel="stylesheet" href="{{URL::asset('UI/css/animate.css')}}" type="text/css"/>



    <link rel="stylesheet" href="{{URL::asset('UI/css/magnific-popup.css')}}" type="text/css"/>



    <!-- Date & Time Picker CSS -->



    <link rel="stylesheet" href="{{URL::asset('UI/css/datepicker.css')}}" type="text/css"/>



    <!-- Hosting Demo Specific Stylesheet -->



    <link rel="stylesheet" href="{{URL::asset('UI/css/course.css')}}" type="text/css"/>



    <link rel="stylesheet" href="{{URL::asset('UI/css/responsive.css')}}" type="text/css"/>



    <meta name="viewport" content="width=device-width, initial-scale=1"/>



    <script>



      //this will hide message after 3 seconds



      setTimeout(function () {



        $("#statusMessage").hide();



      }



                 , 3000)



    </script>



    <!-- Document Title



============================================= -->



    <title>::Welcome to Skillsgroom | Home::



    </title>



  </head>



  <div id="statusMessage">



  </div>



  <body class="stretched" id="pop_up_overflow">



    <!-- Document Wrapper



============================================= -->



    <div id="wrapper" class="clearfix">



      <!-- Header



============================================= -->



      <header id="header" class="transparent-header dark full-header" data-sticky-class="not-dark">

			<div id="header-wrap">

				<div class="container clearfix">

					<div id="primary-menu-trigger"><i class="icon-reorder"></i></div>

					<!-- Logo
					============================================= -->
					<div id="logo">



              <a href="index.php" class="standard-logo" data-dark-logo="{{URL::asset('UI/images/logo-dark.png')}}">



                <img src="{{URL::asset('UI/images/logo.png')}}" alt="Skillsgroom">



              </a>



              <a href="index.php" class="retina-logo" data-dark-logo="{{URL::asset('UI/images/logo-dark@2x.png')}}">



                <img src="{{URL::asset('UI/images/logo-dark.png')}}" alt="Skillsgroom">



              </a>



            </div><!-- #logo end -->

					<!-- Primary Navigation
					============================================= -->
					<nav id="primary-menu">

						<ul>
							<li class="current"><a href="/forum/list"><div>Forum</div></a> </li>
						</ul>

					</nav><!-- #primary-menu end -->

				</div>

			</div>

		</header><!-- #header end -->

		<section id="slider" class="slider-element slider-parallax" style="background: url('https://staging.skillsgroom.com/UI/images/inner_banner.jpg') no-repeat; background-size: cover" data-height-xl="100" data-height-lg="100" data-height-md="100" data-height-sm="100" data-height-xs="100">

		</section>



      <!-- Content



============================================= -->



      <div id="content">



        <div class="container">



          <div id="loginPopup">



            <div class="form-popup" id="popupForm">



              <form action="" class="form-container" method="post" enctype="multipart/form-data">



                <div class="alert alert-success" id="ShowMsgInq" style="display: none;">



                  <strong id="SussMsgInq">



                  </strong>



                </div>



                <h2>Fill the form



                </h2>



                <label for="name">



                  <strong>Name



                  </strong>



                </label>



                <input type="text" id="name" placeholder="Your Name" name="username" required>



                <label for="email">



                  <strong>E-mail Id



                  </strong>



                </label>



                <input type="text" id="email" placeholder="Your Email" name="useremail" required>



                <label for="mobile">



                  <strong>Mobile Number



                  </strong>



                </label>



                <input type="text" id="mobile" placeholder="Your Mobile no" name="usermobile" required>



                <label for="location">



                  <strong>Location



                  </strong>



                </label>



                <input type="text" id="location" placeholder="Your Location" name="userlocation" required>



                <label for="category">



                  <strong>Category



                  </strong>



                </label>



                <select id="category" name="category" required>



                  <option value="General Inquiry" id="General">General inquiry



                  </option>



                  <option value="Want to join as Instructor" id="instructor">Want to join as Instructor or teacher



                  </option>



                  <option value="Music" id="music">Music



                  </option>



                  <option value="Development" id="development">Development



                  </option>



                  <option value="Teacher" id="teacher">Teacher



                  </option>



                  <option value="Food" id="food">Food



                  </option>



                  <option value="Business" id="business">Business



                  </option>



                  <option value="Health fitness" id="health">Health fitness



                  </option>



                  <option value="Lifestyle" id="lifestyle">Lifestyle



                  </option>



                  <option value="Language" id="language">Language



                  </option>



                  <option value="Paint" id="paint">Paint



                  </option>



                  <option value="Photography" id="photography">Photography



                  </option>



                  <option value="Digital marketing" id="digital">Digital marketing



                  </option>



                  <option value="Academics" id="academics">Academics



                  </option>



                </select>



                <label for="details">



                  <strong>Inquiry details



                  </strong>



                </label>



                <textarea type="text" id="inquiry" placeholder="Detailed text of your query or interest"



                          name="userdetails" required>



                </textarea>



                <input type="button" name="send" id="Inquiry" class="btn" value="Submit">



                <button type="button" class="btn cancel" onclick="closeForm()">Close



                </button>



              </form>



            </div>



          </div>



          <!-- Categories



============================================= -->



          {{-- <div class="row topmargin align-items-center divcenter bottommargin-lg" style="max-width: 940px">



            <div class="col-md-3 mb-2 mb-md-0">



              <h3 class="mb-0">Associated Coaching Partners:



              </h3>



            </div>



            <div class="col-md-9" style="z-index: 0;">



              <div id="oc-images" class="owl-carousel image-carousel carousel-widget" data-margin="50" data-nav="false" data-pagi="false" data-items-xs="2" data-items-md="3" data-items-xl="4" data-loop="true" data-autoplay="3500">



                <div class="oc-item">



                  <a href="https://yatriclasses.skillsgroom.com">



                    <img src="https://staging.skillsgroom.com/UI/images/clients/1.jpg" alt="clients" title="click to view">



                  </a>



                  <div class="center" style="height: 80px;">Yatri classes Vadodara,Gujarat B.Com Tuitions



                  </div>



                  <div class="center mt-3">



                    <a href="https://yatriclasses.skillsgroom.com" class="button button-3d button-mini button-rounded button-aqua button-light">More



                    </a>



                  </div>



                </div>



                <div class="oc-item">



                  <a href="https://acceleratedacademy.skillsgroom.com">



                    <img src="https://staging.skillsgroom.com/UI/images/clients/2.jpg" alt="clients" title="click to view">



                  </a>



                  <div class="center" style="height: 80px;">Vadodara,Gujarat Science Tuitions VII to XII



                  </div>



                  <div class="center mt-3">



                    <a href="https://acceleratedacademy.skillsgroom.com" class="button button-3d button-mini button-rounded button-aqua button-light">More



                    </a>



                  </div>



                </div>



                <div class="oc-item">



                  <a href="https://knowledgeseekers.skillsgroom.com">



                    <img src="https://staging.skillsgroom.com/UI/images/clients/3.jpg" alt="clients" title="click to view">



                  </a>



                  <div class="center" style="height: 80px;">Ahmedabad,Gujarat Storyboard /Life Skills teaching



                  </div>



                  <div class="center mt-3">



                    <a href="https://knowledgeseekers.skillsgroom.com" class="button button-3d button-mini button-rounded button-aqua button-light">More



                    </a>



                  </div>



                </div>



                <div class="oc-item">



                  <a href="https://glowwormacademy.skillsgroom.com">



                    <img src="https://staging.skillsgroom.com/UI/images/clients/4.jpg" alt="clients" title="click to view">



                  </a>



                  <div class="center" style="height: 80px;">Ahmedabad,Gujarat ML &amp; Data Science Training



                  </div>



                  <div class="center mt-3">



                    <a href="https://glowwormacademy.skillsgroom.com" class="button button-3d button-mini button-rounded button-aqua button-light">More



                    </a>



                  </div>



                </div>



                <div class="oc-item">



                  <a href="https://abapro.skillsgroom.com">



                    <img src="https://staging.skillsgroom.com/UI/images/clients/5.jpg" alt="clients" title="click to view">



                  </a>



                  <div class="center" style="height: 80px;">Bangalore,Karnataka Business Analysis Training



                  </div>



                  <div class="center mt-3">



                    <a href="https://abapro.skillsgroom.com" class="button button-3d button-mini button-rounded button-aqua button-light">More



                    </a>



                  </div>



                </div>



                <!--<div class="oc-item"><a href="https://www.bulletproofyourstartup.com/"><img src="http://staging.skillsgroom.com/UI/images/clients/6.jpg" alt="clients" title="click to view"></a>-->



                <!--<div class="center" style="height: 80px;">Bulletproof Bangalore,Startups Coaching</div>-->



                <!--	<div class="center mt-3"><a href="https://www.bulletproofyourstartup.com" class="button button-3d button-mini button-rounded button-aqua button-light">More</a></div>-->



                <!--</div>-->



                <div class="oc-item">



                  <a href="https://skillsgroom.com/sites/vikasgupta">



                    <img src="https://staging.skillsgroom.com/UI/images/clients/7.jpg" alt="clients" title="click to view">



                  </a>



                  <div class="center" style="height: 80px;">New Delhi,Guitar Certification Courses London



                  </div>



                  <div class="center mt-3">



                    <a href="https://skillsgroom.com/sites/vikasgupta" class="button button-3d button-mini button-rounded button-aqua button-light">More



                    </a>



                  </div>



                </div>



              </div>



            </div>



          </div> --}}



          <div class="clear">



          </div>



        </div>



      <div class="section my-0" style="padding: 40px 0; background-image: url('{{URL::asset('UI/images/course_bg.jpg')}}'); background-size: auto; background-repeat: repeat">

          <div class="container">
            <form action="/search_course" method="GET">
                @csrf
            <div class="input-group divcenter mb-4">

                  <div class="input-group-prepend">
                      <div class="input-group-text"><i class="icon-email2"></i></div>
                  </div>
                  <input type="text" id="widget-subscribe-form-email" name="course_name" class="form-control required email" placeholder="Type subject or topic keyword to search">
                  <div class="input-group-append">
                      <button class="btn btn-success" type="submit">Search</button>
                  </div>

            </div>
        </form>


            <div class="heading-block nobottomborder mb-4 center">



              <h3 class="text-white">Online Courses and Events



              </h3>



            </div>



            <div class="clear">



            </div>



            <div class="row mt-2">



              <!-- Course 1



============================================= -->



              @if(isset($Courses))



              @foreach($Courses as $Course)







              <div class="col-md-4 mb-5">



                <div class="card course-card hover-effect noborder">



                  <a href="course_details/{{$Course['id']}}" target="_blank">



                    @if($Course['course_image'])



                  <img class="card-img-top" src="https://onlinelms.skillsgroom.com/UI/courses/{{$Course['course_image']}}" alt="Card image cap" />



                    @else



                      <img class="card-img-top" src="https://onlinelms.skillsgroom.com/UI/images/1.jpg" alt="Card image cap" />



                    @endif



                  </a>



                  <div class="card-body">



                    <h4 class="card-title t700 mb-2">



                      <a href="course_details/{{$Course['id']}}" target="_blank">{{$Course['title']}}



                      </a>



                    </h4>



                    <p class="mb-2 card-title-sub uppercase t400 ls1">



                      <a href="#" class="text-black-50">



                        @if($Course['category'] == 1)



                          Tuition & support classes



                        @elseif($Course['category'] == 2)



                          Hobbies & Activities



                        @elseif($Course['category'] == 3)



                          Technology Trainings



                        @elseif($Course['category'] == 4)



                          Certification Courses



                        @elseif($Course['category'] == 5)



                          Events & seminars



                        @elseif($Course['category'] == 6)



                          Exam and Test



                        @elseif($Course['category'] == 7)



                          Job interview preparation



                        @elseif($Course['category'] == 8)



                          Mentorship & coaching



                        @endif



                      </a>



                    </p>



                    {{-- <p class="card-text text-black-50 mb-1">{!! Str::words($Course['course_description'], 10 ) !!} </p> --}}



                    <p class="card-text text-black-50 mb-1" style="height: 60px;">{{$Course['who_should_join']}}</p>



                  </div>



                  <div class="card-footer py-3 d-flex justify-content-between align-items-center bg-white text-muted">



                    <div class="badge alert-primary"> @if(isset($Course['course_date']))
                        {{date('d M Y', strtotime($Course['course_date']))}}
                    @else

                    @endif



                    </div>



                    <div class="badge alert-warning">Duration : {{$Course['duration']}}



                    </div>



                  </div>



                </div>



              </div>



              @endforeach



              @endif



              <div class="center" style="width: 100%;">



                <a href="course_list" class="btn button-amber ml-4 mt-2">View All Courses



                </a>



              </div>



            </div>



          </div>



        </div>

        <div class="container">
            <br>
            <br>
            <div class="section-title">
                <h2>Teachers I Speakers I Coaches</h2>
                <!--<p>Our Professional Trainers</p>-->
              </div>
              <div class="row">

                <div id="oc-images" class="owl-carousel image-carousel carousel-widget" data-margin="20" data-autoplay="3000" data-nav="true" data-loop="true" data-pagi="false" data-items-xs="1" data-items-sm="2" data-items-lg="3" data-items-xl="4">
                    @if(isset($Teachers))
                    @foreach($Teachers as $Teacher)
                            <div class="oc-item">
                                @if(isset($Teachers))
                                <img src="https://onlinelms.skillsgroom.com/UI/teachers_profile_pic/{{ $Teacher['profile_pic'] }}" alt="Image 1" class="cropped" title="{{ $Teacher['first_name'] }} {{ $Teacher['last_name'] }}">
                                @else
                                <img src="https://onlinelms.skillsgroom.com/UI/teachers_profile_pic/5f9d7052e116c_1604153426.jpg" alt="Image 1" class="cropped" title="{{ $Teacher['first_name'] }} {{ $Teacher['last_name'] }}">
                                @endif
                                <div class="member member-content no-shadow">
                                    <a href="advisor_details/{{ $Teacher['id'] }}" target="_blank">
                                        <h4>{{ $Teacher['first_name'] }} {{ $Teacher['last_name'] }}</h4>
                                    </a>
                    <span>{{ $Teacher['teaching_subjects'] }}</span>
                                </div>
                            </div>
                            @endforeach
                            @endif
                        </div>


            </div>

            <br><br>

        </div>

        {{-- <div class="container">



          <div class="ad_image mt-0">



            <a href="https://skillsgroom.com/sites/hobbyclasses">



              <img src="https://staging.skillsgroom.com/UI/images/chess_banner.jpg" alt="" />



            </a>



          </div>



          <div class="ad_image">



            <a href="https://skillsgroom.com/sites/hobbyclasses">



              <img src="https://staging.skillsgroom.com/UI/images/science_banner.jpg" alt="" />



            </a>



          </div>



          <div class="ad_image">



            <a href="https://skillsgroom.com/sites/hobbyclasses">



              <img src="https://staging.skillsgroom.com/UI/images/robotics_banner.jpg" alt="" />



            </a>



          </div>



        </div> --}}



        <!-- ======= Trainers Section ======= -->



        {{-- <section id="trainers" class="trainers">



          <div class="container">



            <div class="section-title">



              <h2>Live Online Classes and Events



              </h2>



              <!--<p>Live Online Classes and Events</p>-->



            </div>



            <div class="row">



              <div class="col-lg-3 col-md-6 d-flex align-items-stretch">



                <div class="member">



                  <img src="https://staging.skillsgroom.com/UI/images/demo/music1.jpg" class="img-fluid" alt="">



                  <div class="member-content">



                    <h4>Harmonium



                    </h4>



                    <span>Beginners



                    </span>



                    <p>



                      Per Session -Rs.249



                    </p>



                    <a href="/main/book_demo" target="_blank" class="button button-3d button-small button-rounded button-amber ls1 ls1 nott">Book Free Demo Class



                    </a>



                  </div>



                </div>



              </div>



              <div class="col-lg-3 col-md-6 d-flex align-items-stretch">



                <div class="member">



                  <img src="https://staging.skillsgroom.com/UI/images/demo/music9.jpg" class="img-fluid" alt="">



                  <div class="member-content">



                    <h4>Chess



                    </h4>



                    <span>Beginners



                    </span>



                    <p>



                      Per Session -Rs.279



                    </p>



                    <a href="/main/book_demo" target="_blank" class="button button-3d button-small button-rounded button-amber ls1 ls1 nott">Book Free Demo Class



                    </a>



                  </div>



                </div>



              </div>



              <div class="col-lg-3 col-md-6 d-flex align-items-stretch">



                <div class="member">



                  <img src="https://staging.skillsgroom.com/UI/images/demo/music10.jpg" class="img-fluid" alt="">



                  <div class="member-content">



                    <h4>Keyboard



                    </h4>



                    <span>Beginners



                    </span>



                    <p>



                      Per Session -Rs.299



                    </p>



                    <a href="/main/book_demo" target="_blank" class="button button-3d button-small button-rounded button-amber ls1 ls1 nott">Book Free Demo Class



                    </a>



                  </div>



                </div>



              </div>



              <div class="col-lg-3 col-md-6 d-flex align-items-stretch">



                <div class="member">



                  <img src="https://staging.skillsgroom.com/UI/images/demo/music4.jpg" class="img-fluid" alt="">



                  <div class="member-content">



                    <h4>Guitar



                    </h4>



                    <span>Certification Course



                    </span>



                    <p>



                      Trinity College London



                    </p>



                    <a href="/main/book_demo" target="_blank" class="button button-3d button-small button-rounded button-amber ls1 ls1 nott">Book Free Demo Class



                    </a>



                  </div>



                </div>



              </div>



              <div class="col-lg-3 col-md-6 d-flex align-items-stretch">



                <div class="member">



                  <img src="https://staging.skillsgroom.com/UI/images/demo/music5.jpg" class="img-fluid" alt="">



                  <div class="member-content">



                    <h4>Maths Tuitions



                    </h4>



                    <span>Grade VII to IX, CBSE/ICSE



                    </span>



                    <p>



                      Starting per Hour - Rs.129



                    </p>



                    <a href="/main/book_demo" target="_blank" class="button button-3d button-small button-rounded button-amber ls1 ls1 nott">Book Free Demo Class



                    </a>



                  </div>



                </div>



              </div>



              <div class="col-lg-3 col-md-6 d-flex align-items-stretch">



                <div class="member">



                  <img src="https://staging.skillsgroom.com/UI/images/demo/music6.jpg" class="img-fluid" alt="">



                  <div class="member-content">



                    <h4>Science Tuitions



                    </h4>



                    <span>Grade VII to IX, CBSE/ICSE



                    </span>



                    <p>



                      Starting per Hour  - Rs.249



                    </p>



                    <a href="/main/book_demo" target="_blank" class="button button-3d button-small button-rounded button-amber ls1 ls1 nott">Book Free Demo Class



                    </a>



                  </div>



                </div>



              </div>



              <div class="col-lg-3 col-md-6 d-flex align-items-stretch">



                <div class="member">



                  <img src="https://staging.skillsgroom.com/UI/images/demo/music7.jpg" class="img-fluid" alt="">



                  <div class="member-content">



                    <h4>Maths Tuitions



                    </h4>



                    <span>Grade X to XII, CBSE/ICSE



                    </span>



                    <p>



                      Starting per Hour  - Rs.299



                    </p>



                    <a href="/main/book_demo" target="_blank" class="button button-3d button-small button-rounded button-amber ls1 ls1 nott">Book Free Demo Class



                    </a>



                  </div>



                </div>



              </div>



              <div class="col-lg-3 col-md-6 d-flex align-items-stretch">



                <div class="member">



                  <img src="https://staging.skillsgroom.com/UI/images/demo/music8.jpg" class="img-fluid" alt="">



                  <div class="member-content">



                    <h4 style="font-size: 17px;">Physics, Chemistry,Biology



                    </h4>



                    <span>Grade  X to XII, CBSE/ICSE



                    </span>



                    <p>



                      Starting per Hour  - Rs.299



                    </p>



                    <a href="/main/book_demo" target="_blank" class="button button-3d button-small button-rounded button-amber ls1 ls1 nott">Book Free Demo Class



                    </a>



                  </div>



                </div>



              </div>



            </div>



          </div>



        </section> --}}



        <!-- End Trainers Section -->



        {{-- <div class="section mt-0 mb-0 pt-4 trainers">



          <div class="container" style="z-index: 0;">



            <div class="row justify-content-between align-items-center clearfix">



              <div class="col-lg-3 col-sm-6">



                <div class="feature-box fbox-right noborder">



                  <div class="member">



                    <img src="https://staging.skillsgroom.com/UI/images/extra_courses/img_1.png" class="img-fluid" alt="">



                    <div class="member-content">



                      <h5>The 7 Skills of Critical Thinking



                      </h5>



                      <p class="pb-2 pt-0 mb-0">Business Innovation



                      </p>



                    </div>



                  </div>



                </div>



                <div class="feature-box fbox-right noborder mt-5">



                  <div class="member">



                    <img src="https://staging.skillsgroom.com/UI/images/extra_courses/img_2.png" class="img-fluid" alt="">



                    <div class="member-content">



                      <h5>The Four Types Of Leader



                      </h5>



                      <p class="pb-2 pt-0 mb-0">Leadership Essentials



                      </p>



                    </div>



                  </div>



                </div>



              </div>



              <div class="col-lg-5 col-7 offset-3 offset-sm-0 d-sm-none d-lg-block center my-1">



                <div class="heading-block nobottomborder center divcenter mb-0 clearfix">



                  <h3 class="nott ls0 mb-1" style="font-size: 25px">Looking for Business Skills Learning ?



                  </h3>



                  <p class="mb-4" style="font-size: 14px">Subscribe now and learn  60+ Business key skills at your convenience



                  </p>



                </div>



                <a href="https://rzp.io/l/8xeZySeT">



                  <img src="https://staging.skillsgroom.com/UI/images/offer_img.jpg" alt="offer" class="rounded parallax" data-bottom-top="transform: translateY(-20px)" data-top-bottom="transform: translateY(20px)" width="250">



                </a>



                <br>



                <p class="mt-4" style="text-align: left;">



                  <b>Best benefits:



                  </b>



                </p>



                <ul class="iconlist iconlist-large iconlist-color pb-0" style="height: auto; text-align: left;">



                  <li>



                    <i class="icon-ok-sign">



                    </i> Content developed by  well qualified professionals



                  </li>



                  <li>



                    <i class="icon-ok-sign">



                    </i> All the topics are of great importance for any Industry and Professional



                  </li>



                  <li>



                    <i class="icon-ok-sign">



                    </i> Learn at your time slots.



                  </li>



                  <li>



                    <i class="icon-ok-sign">



                    </i> All videos are 10-15 minutes duration and you can easily manage with your schedule



                  </li>



                  <li>



                    <i class="icon-ok-sign">



                    </i> eCertificate at the end of every course you complete



                  </li>



                </ul>



                <p style="text-align: left; font-size: 13px;color:#ff0000;">



                  <i>** - This offer price is available only till 30th November 2020



                    <br>



                    ** - Validity for 3 Months



                  </i>



                </p>



                <a href="https://skillsgroom.talentlms.com/catalog" class="btn button-aqua ml-4 mt-2">View All Courses



                </a>



              </div>



              <div class="col-lg-3 col-sm-6">



                <div class="feature-box noborder">



                  <div class="member">



                    <img src="https://staging.skillsgroom.com/UI/images/extra_courses/img_3.png" class="img-fluid" alt="">



                    <div class="member-content">



                      <h5>The Importance of Training



                      </h5>



                      <p class="pb-2 pt-0 mb-0">HR Essentials



                      </p>



                    </div>



                  </div>



                </div>



                <div class="feature-box noborder mt-5">



                  <div class="member">



                    <img src="https://staging.skillsgroom.com/UI/images/extra_courses/img_4.png" class="img-fluid" alt="">



                    <div class="member-content">



                      <h5>Sales Listening Skills



                      </h5>



                      <p class="pb-2 pt-0 mb-0">Sales Essentials



                      </p>



                    </div>



                  </div>



                </div>



              </div>



            </div>



          </div>



        </div> --}}



        <!-- ======= Upcoming Section ======= -->



        {{-- <section>



          <div class="container clearfix">



            <div class="section-title">



              <h2>Upcoming Events



              </h2>



              <!-- <p>Our Features</p>-->



            </div>



            <div class="row">



              <div class="entry event col-6">



                <div class="grid-inner row align-items-center no-gutters">



                  <div class="entry-image col-md-5 mb-md-0">



                    <img src="https://staging.skillsgroom.com/UI/images/upcoming/3_upcoming.jpg" alt="Inventore voluptates velit totam ipsa tenetur">



                    <div class="entry-date" style="width: 95px;">7&amp;8



                      <span>Nov



                      </span>



                    </div>



                  </div>



                  <div class="col-md-7 pl-md-4">



                    <div class="entry-title title-xs">



                      <h2>



                        <a href="#">Data Science and Machine Learning



                        </a>



                      </h2>



                    </div>



                    <div class="entry-meta">



                      <ul>



                        <li>



                          <i class="icon-time">



                          </i>5 Hours



                        </li>



                        <li>



                          <i class="icon-map-marker2">



                          </i>Online workshop



                        </li>



                      </ul>



                    </div>



                    <div class="entry-content">



                      <div class="btn btn-info mt-2 mr-2" style="cursor: unset;">Fee - Rs. 500



                      </div>



                      <a href="https://rzp.io/l/FXn5uXnE" class="btn btn-danger mt-2">Register Now



                      </a>



                    </div>



                  </div>



                </div>



              </div>



              <div class="entry event col-6">



                <div class="grid-inner row align-items-center no-gutters">



                  <div class="entry-image col-md-5 mb-md-0">



                    <img src="https://staging.skillsgroom.com/UI/images/upcoming/2_upcoming.jpg" alt="Nemo quaerat nam beatae iusto minima vel">



                    <div class="entry-date" style="width: 95px;">7&amp;8



                      <span>Nov



                      </span>



                    </div>



                  </div>



                  <div class="col-md-7 pl-md-4">



                    <div class="entry-title title-xs">



                      <h2>



                        <a href="#">Leadership Mania



                        </a>



                      </h2>



                    </div>



                    <div class="entry-meta">



                      <ul>



                        <li class="mb-1">



                          <i class="icon-time">



                          </i> 10 AM to 12 PM IST



                        </li>



                        <li class="mb-1" style="line-height: 19px;">



                          <i class="icon-user">



                          </i>



                          <b>Ms.Rushika Hathi



                          </b> - Author ,Motivational Speaker,Corporate Trainer & Life Coach



                        </li>



                        <li class="mb-1">



                          <i class="icon-map-marker2">



                          </i> 2 Days online workshop for Juniors (Age 10 to 14 yrs)



                        </li>



                      </ul>



                    </div>



                    <div class="entry-content">



                      <div class="btn btn-info mt-2 mr-2" style="cursor: unset;">Fee - Rs. 500



                      </div>



                      <a href="https://rzp.io/l/dXxqrYi" class="btn btn-danger mt-2">Register Now



                      </a>



                    </div>



                  </div>



                </div>



              </div>



            </div>



          </div>



        </section> --}}



        <!-- ======= Features Section ======= -->



        {{-- <section class="feature_area pt-120 pb-130 courses" id="popular-courses">



          <div class="container">



            <div class="section-title">



              <!--<h2>Features</h2>-->



              <p>Why us ?



              </p>



            </div>



            <ul class="iconlist iconlist-large iconlist-color" style="height: auto;">



              <li>



                <i class="icon-ok-sign">



                </i> 100% focus on quality learning



              </li>



              <li>



                <i class="icon-ok-sign">



                </i> We assign best teachers / coaches based on your need



              </li>



              <li>



                <i class="icon-ok-sign">



                </i> 24X7 support services



              </li>



              <li>



                <i class="icon-ok-sign">



                </i> Best pricing offers



              </li>



            </ul>



            <div class="row">



              <!--Schedule Carousel-->



              <div class="col-md-5">



                <div id="oc-posts" class="owl-carousel posts-carousel carousel-widget" data-margin="20" data-autoplay="4000" data-nav="true" data-loop="true" data-pagi="false" data-items-xs="1" data-items-sm="1" data-items-md="1" data-items-lg="1" data-items-xl="1">



                  <!--Schedule Item 1-->



                  <div class="oc-item">



                    <div class="ipost clearfix">



                      <div class="course-item">



                        <div>



                          <!--share icon-->



                          <img src="https://staging.skillsgroom.com/UI/images/schedule/course-5.jpg" class="cropped_schedule" alt="...">



                        </div>



                        <div class="course-content">



                          <h3>



                            <a href="https://glowwormacademy.skillsgroom.com">Data Science &amp; Machine Learning Certification Training



                            </a>



                            <br>



                            <span>by Glowworm Academy, Ahmedabad, Gujarat



                            </span>



                          </h3>



                          <ul class="iconlist">



                            <li>



                              <i class="icon-book1">



                              </i> Projects + Internship for beginners



                            </li>



                            <li>



                              <i class="icon-book1">



                              </i> Career development for professionals



                            </li>



                            <li>



                              <i class="icon-book1">



                              </i> Handholding to make you Job ready



                            </li>



                          </ul>



                        </div>



                      </div>



                    </div>



                  </div>



                  <!--Schedule Item 2-->



                  <div class="oc-item">



                    <div class="ipost clearfix">



                      <div class="course-item">



                        <div>



                          <!--share icon-->



                          <img src="https://staging.skillsgroom.com/UI/images/schedule/course-6.jpg" class="cropped_schedule" alt="...">



                        </div>



                        <div class="course-content">



                          <h3>



                            <a href="https://wa.me/918618414801?text=Hi,%20I%20am%20interested%20in%20a%20free%20demo%20class%20for%20your%20course/event%20mentioned%20at%20skillsgroom.com.%20Please%20share%20more%20details%20or%20call%20me%20back.%20Thanks.">Drum Classes



                            </a>



                            <br>



                            <span>by Abhishek Raj Sinha, Delhi, NCR



                            </span>



                          </h3>



                          <ul class="iconlist">



                            <li>



                              <i class="icon-book1">



                              </i>10+ years of teaching experience



                            </li>



                            <li>



                              <i class="icon-book1">



                              </i>Professional Drummer



                            </li>



                            <li>



                              <i class="icon-book1">



                              </i>1 to 1 or Group teaching



                            </li>



                          </ul>



                        </div>



                      </div>



                    </div>



                  </div>



                  <!--Schedule Item 3-->



                  <div class="oc-item">



                    <div class="ipost clearfix">



                      <div class="course-item">



                        <div>



                          <!--share icon-->



                          <img src="https://staging.skillsgroom.com/UI/images/schedule/course-1.jpg" class="cropped_schedule" alt="...">



                        </div>



                        <div class="course-content">



                          <h3>



                            <a href="https://wa.me/918618414801?text=Hi,%20I%20am%20interested%20in%20a%20free%20demo%20class%20for%20your%20course/event%20mentioned%20at%20skillsgroom.com.%20Please%20share%20more%20details%20or%20call%20me%20back.%20Thanks.">Accounting Coaching for Business Owners



                            </a>



                            <br>



                            <span>by Accountswale, Bangalore, Karnataka



                            </span>



                          </h3>



                          <ul class="iconlist">



                            <li>



                              <i class="icon-book1">



                              </i> One to one coaching  by Mr.Suresh Vankar



                            </li>



                            <li>



                              <i class="icon-book1">



                              </i> 20+ years experience in working with startups



                            </li>



                            <li>



                              <i class="icon-book1">



                              </i> Expert in setting up efficient operations &amp; Finance team



                            </li>



                          </ul>



                        </div>



                      </div>



                    </div>



                  </div>



                  <!--Schedule Item 4-->



                  <div class="oc-item">



                    <div class="ipost clearfix">



                      <div class="course-item">



                        <div>



                          <img src="https://staging.skillsgroom.com/UI/images/schedule/course-2.jpg" class="cropped_schedule" alt="...">



                        </div>



                        <div class="course-content">



                          <h3>



                            <a href="https://abapro.skillsgroom.com">Business Analyst Training



                            </a>



                            <br>



                            <span>by ABA Pro, Bangalore, Karnataka



                            </span>



                          </h3>



                          <ul class="iconlist">



                            <li>



                              <i class="icon-book1">



                              </i> Great career opportunities in Business Analysis



                            </li>



                            <li>



                              <i class="icon-book1">



                              </i> Practical training by Senior professionals



                            </li>



                            <li>



                              <i class="icon-book1">



                              </i> Every industry needs Business Analyst



                            </li>



                          </ul>



                        </div>



                      </div>



                    </div>



                  </div>



                  <!--Schedule Item 5-->



                  <div class="oc-item">



                    <div class="ipost clearfix">



                      <div class="course-item">



                        <div>



                          <img src="https://staging.skillsgroom.com/UI/images/schedule/course-3.jpg" class="cropped_schedule" alt="...">



                        </div>



                        <div class="course-content">



                          <h3>



                            <a href="https://wa.me/918618414801?text=Hi,%20I%20am%20interested%20in%20a%20free%20demo%20class%20for%20your%20course/event%20mentioned%20at%20skillsgroom.com.%20Please%20share%20more%20details%20or%20call%20me%20back.%20Thanks.">Yoga for Healthy Living



                            </a>



                            <br>



                            <span>by Shri Sarju Karia, Rajkot, Gujarat



                            </span>



                          </h3>



                          <ul class="iconlist">



                            <li>



                              <i class="icon-book1">



                              </i> Join him for Daily Yoga Practice



                            </li>



                            <li>



                              <i class="icon-book1">



                              </i> Learn from Yogi and Spiritual leader



                            </li>



                            <li>



                              <i class="icon-book1">



                              </i> Language of teaching -Hindi &amp; Gujarati



                            </li>



                          </ul>



                        </div>



                      </div>



                    </div>



                  </div>



                  <!--Schedule Item 6-->



                  <div class="oc-item">



                    <div class="ipost clearfix">



                      <div class="course-item">



                        <div>



                          <img src="https://staging.skillsgroom.com/UI/images/schedule/course-4.jpg" class="cropped_schedule" alt="...">



                        </div>



                        <div class="course-content">



                          <h3>



                            <a href="https://yatriclasses.skillsgroom.com">Tuitions for B.Com and CMA



                            </a>



                            <br>



                            <span>by Yatri Classes, Vadodara, Gujarat



                            </span>



                          </h3>



                          <ul class="iconlist">



                            <li>



                              <i class="icon-book1">



                              </i> 18+ years of Teaching Experience



                            </li>



                            <li>



                              <i class="icon-book1">



                              </i> 10000+ Classes Delivered



                            </li>



                            <li>



                              <i class="icon-book1">



                              </i> Specialization in Accountancy &amp; Statistics



                            </li>



                          </ul>



                        </div>



                      </div>



                    </div>



                  </div>



                  <!--Schedule Item 7-->



                  <div class="oc-item">



                    <div class="ipost clearfix">



                      <div class="course-item">



                        <div>



                          <img src="https://staging.skillsgroom.com/UI/images/schedule/course-7.jpg" class="cropped_schedule" alt="...">



                        </div>



                        <div class="course-content">



                          <h3>



                            <a href="https://acceleratedacademy.skillsgroom.com">Tuitions for Science - Grade VII to IX



                            </a>



                            <br>



                            <span>by Accelerated Academy, Vadodara, Gujarat



                            </span>



                          </h3>



                          <ul class="iconlist">



                            <li>



                              <i class="icon-book1">



                              </i> 10+ years Teaching Experience



                            </li>



                            <li>



                              <i class="icon-book1">



                              </i> Giving tuitions for CBSE Syllabus



                            </li>



                            <li>



                              <i class="icon-book1">



                              </i> Interactive teaching methods



                            </li>



                          </ul>



                        </div>



                      </div>



                    </div>



                  </div>



                  <!--Schedule Item 8-->



                  <div class="oc-item">



                    <div class="ipost clearfix">



                      <div class="course-item">



                        <div>



                          <img src="https://staging.skillsgroom.com/UI/images/schedule/course-8.jpg" class="cropped_schedule" alt="...">



                        </div>



                        <div class="course-content">



                          <h3>



                            <a href="https://acceleratedacademy.skillsgroom.com">Tuitions Physics- Grade X to XII



                            </a>



                            <br>



                            <span>by Accelerated Academy, Vadodara, Gujarat



                            </span>



                          </h3>



                          <ul class="iconlist">



                            <li>



                              <i class="icon-book1">



                              </i> 10+ years Teaching Experience



                            </li>



                            <li>



                              <i class="icon-book1">



                              </i> Giving tuitions for CBSE Syllabus



                            </li>



                            <li>



                              <i class="icon-book1">



                              </i> Focused learning &amp; NEET preperation



                            </li>



                          </ul>



                        </div>



                      </div>



                    </div>



                  </div>



                  <!--Schedule Item 9-->



                  <div class="oc-item">



                    <div class="ipost clearfix">



                      <div class="course-item">



                        <div>



                          <img src="https://staging.skillsgroom.com/UI/images/schedule/course-9.jpg" class="cropped_schedule" alt="...">



                        </div>



                        <div class="course-content">



                          <h3>



                            <a href="https://harishverma.skillsgroom.com">Music Hobby Classes



                            </a>



                            <br>



                            <span>by Prof. Harish Verma,Varanasi, UP



                            </span>



                          </h3>



                          <ul class="iconlist">



                            <li>



                              <i class="icon-book1">



                              </i> 15+ years of Teaching Experience



                            </li>



                            <li>



                              <i class="icon-book1">



                              </i> Professional performer



                            </li>



                            <li>



                              <i class="icon-book1">



                              </i> Specialization in Guitar, Violin and Harmonium



                            </li>



                          </ul>



                        </div>



                      </div>



                    </div>



                  </div>



                  <!--Schedule Item 10-->



                  <div class="oc-item">



                    <div class="ipost clearfix">



                      <div class="course-item">



                        <div>



                          <img src="https://staging.skillsgroom.com/UI/images/schedule/course-10.jpg" class="cropped_schedule" alt="...">



                        </div>



                        <div class="course-content">



                          <h3>



                            <a href="https://vikasgupta.skillsgroom.com">Guitar -Graded Course, Trinity College London



                            </a>



                            <br>



                            <span>by Vikas Gupta, Ghaziabad, NCR, UP



                            </span>



                          </h3>



                          <ul class="iconlist">



                            <li>



                              <i class="icon-book1">



                              </i> 10+ years of Guitar professional teaching



                            </li>



                            <li>



                              <i class="icon-book1">



                              </i> 6+ Syllabus teaching



                            </li>



                            <li>



                              <i class="icon-book1">



                              </i> Certification and grades as per Trinity College London



                            </li>



                          </ul>



                        </div>



                      </div>



                    </div>



                  </div>



                  <!--Schedule Item 11-->



                  <!--<div class="oc-item">





                  <!--Schedule Item 12-->



                  <div class="oc-item">



                    <div class="ipost clearfix">



                      <div class="course-item">



                        <div>



                          <img src="https://staging.skillsgroom.com/UI/images/schedule/course-12.jpg" class="cropped_schedule" alt="...">



                        </div>



                        <div class="course-content">



                          <h3>



                            <a href="https://knowledgeseekers.skillsgroom.com">Grammar from grassroots, StoryBoard concepts



                            </a>



                            <br>



                            <span>by Knowledge Seekers, Ahmedabad, Gujarat



                            </span>



                          </h3>



                          <ul class="iconlist">



                            <li>



                              <i class="icon-book1">



                              </i> 10+ years of life skills teaching experience



                            </li>



                            <li>



                              <i class="icon-book1">



                              </i> Unique teaching concepts



                            </li>



                            <li>



                              <i class="icon-book1">



                              </i> Creative thinking &amp; writing for school kids



                            </li>



                          </ul>



                        </div>



                      </div>



                    </div>



                  </div>



                </div>



                <!--Schedule Ends-->



              </div>



              <div class="clear">



              </div>



            </div>



          </div>



          <div class="feature_image d-none d-lg-table">



            <div class="image">



              <img src="https://staging.skillsgroom.com/UI/images/choose_bg.png" alt="">



            </div>



          </div>



          <div class="ad_image">



            <img src="https://staging.skillsgroom.com/UI/images/ad_banner.png" alt="" />



          </div>



        </section> --}}



        <!-- Features



============================================= -->



        {{-- <div class="section mt-0 mb-0 pb-0 pt-4 courses">



          <div class="container" style="z-index: 0;">



            <div class="heading-block nobottomborder center divcenter mb-0 clearfix" style="max-width: 700px">



              <h3 class="nott ls0 mb-3">Are you running Tuition Classes ?



              </h3>



              <p>Partner with us today and use our best in class LMS 100% free ( Free upto 30 Students)



              </p>



            </div>



            <div class="row justify-content-between align-items-center clearfix">



              <div class="col-lg-4 col-sm-6">



                <div class="feature-box fbox-right noborder">



                  <h3 class="nott ls0">Create Tuition Batches



                  </h3>



                  <p>Easily create and manage Batches as per your Tuition Timing. Register your students under their respective batch.



                  </p>



                </div>



                <div class="feature-box fbox-right noborder mt-5">



                  <h3 class="nott ls0">Make students groups



                  </h3>



                  <p>Create students groups based on their grades /class and organise all communications.



                  </p>



                </div>



                <div class="feature-box fbox-right noborder mt-5">



                  <h3 class="nott ls0">Share files and Video recordings



                  </h3>



                  <p>Unlimited Video and File storage. Give access to only View or download based on need.  Edit /Remove access in seconds.



                  </p>



                </div>



              </div>



              <div class="col-lg-3 col-7 offset-3 offset-sm-0 d-sm-none d-lg-block center my-5">



                <a href="/partners">



                  <img src="https://staging.skillsgroom.com/UI/images/tuition.png" alt="iphone" class="rounded  parallax" data-bottom-top="transform: translateY(-30px)" data-top-bottom="transform: translateY(30px)">



                </a>



                <a href="/main/partners" class="button button-3d button-small button-rounded button-aqua ls1 ls1 nott">Partner With Us



                </a>



                </button>



            </div>



            <div class="col-lg-4 col-sm-6">



              <div class="feature-box noborder">



                <h3 class="nott ls0">Create test papers



                </h3>



                <p>Prepare online test papers with the option of MCQ, Fill in the blank or Descriptive. Ease the flow with zero manual work.



                </p>



              </div>



              <div class="feature-box noborder mt-5">



                <h3 class="nott ls0">Conduct online tests



                </h3>



                <p>Schedule online tests and create rules, Quickly check answers and publish results.



                </p>



              </div>



              <div class="feature-box noborder mt-5">



                <h3 class="nott ls0">Give assignments online



                </h3>



                <p>On a click , create an assignment or upload files and assign it to students . Manage assignment submission and checking online with simple steps.



                </p>



              </div>



            </div>



          </div>



        </div> --}}



      </div>



      <!-- ======= Trainers Section ======= -->



      {{-- <section id="trainers" class="trainers">



        <div class="container" style="z-index: 0;">



          <div class="section-title">



            <h2>Teachers I Speakers I Coaches



            </h2>



            <!--<p>Our Professional Trainers</p>-->



          </div>



          <div class="row">



            <div id="oc-images" class="owl-carousel image-carousel carousel-widget" data-margin="20" data-autoplay="3000" data-nav="true" data-loop="true" data-pagi="false" data-items-xs="1" data-items-sm="2" data-items-lg="3" data-items-xl="4">



              <div class="oc-item">



                <a href="https://yatriclasses.skillsgroom.com">



                  <img src="https://staging.skillsgroom.com/UI/images/trainers/male.jpg" alt="Image 1" class="cropped" title="Click to view">



                </a>



                <div class="member member-content no-shadow">



                  <h4>Hetal Patel



                  </h4>



                  <span>Lecturer



                  </span>



                </div>



                <div class="center mt-3">



                  <a href="https://yatriclasses.skillsgroom.com" class="button button-3d button-mini button-rounded button-aqua button-light">Reach Me



                  </a>



                </div>



              </div>



              <div class="oc-item">



                <a href="https://acceleratedacademy.skillsgroom.com">



                  <img src="https://staging.skillsgroom.com/UI/images/trainers/trainer-7.jpg" alt="Image 2" class="cropped" title="Click to view">



                </a>



                <div class="member member-content no-shadow">



                  <h4>Neelesh Upadhyay



                  </h4>



                  <span>Science Teacher



                  </span>



                </div>



                <div class="center mt-3">



                  <a href="https://acceleratedacademy.skillsgroom.com" class="button button-3d button-mini button-rounded button-aqua button-light">Reach Me



                  </a>



                </div>



              </div>



              <div class="oc-item">



                <a href="https://acceleratedacademy.skillsgroom.com">



                  <img src="https://staging.skillsgroom.com/UI/images/trainers/trainer-8.jpg" alt="Image 3" class="cropped" title="Click to view">



                </a>



                <div class="member member-content no-shadow">



                  <h4>T. Bhaskar Rao



                  </h4>



                  <span>Chemistry Teacher



                  </span>



                </div>



                <div class="center mt-3">



                  <a href="https://acceleratedacademy.skillsgroom.com" class="button button-3d button-mini button-rounded button-aqua button-light">Reach Me



                  </a>



                </div>



              </div>



              <div class="oc-item">



                <a href="https://knowledgeseekers.skillsgroom.com">



                  <img src="https://staging.skillsgroom.com/UI/images/trainers/trainer-12.jpg" alt="Image 4" class="cropped" title="Click to view">



                </a>



                <div class="member member-content no-shadow">



                  <h4>Ekta Desai



                  </h4>



                  <span>Life Skills Coach



                  </span>



                </div>



                <div class="center mt-3">



                  <a href="https://knowledgeseekers.skillsgroom.com" class="button button-3d button-mini button-rounded button-aqua button-light">Reach Me



                  </a>



                </div>



              </div>



              <div class="oc-item">



                <a href="https://glowwormacademy.skillsgroom.com">



                  <img src="https://staging.skillsgroom.com/UI/images/trainers/trainer-4.jpg" alt="Image 5" class="cropped" title="Click to view">



                </a>



                <div class="member member-content no-shadow">



                  <h4>Aditya Shah



                  </h4>



                  <span>ML &amp; Data Science Expert



                  </span>



                </div>



                <div class="center mt-3">



                  <a href="https://glowwormacademy.skillsgroom.com" class="button button-3d button-mini button-rounded button-aqua button-light">Reach Me



                  </a>



                </div>



              </div>



              <div class="oc-item">



                <a href="https://abapro.skillsgroom.com/">



                  <img src="https://staging.skillsgroom.com/UI/images/trainers/trainer-2.jpg" alt="Image 6" class="cropped" title="Click to view">



                </a>



                <div class="member member-content no-shadow">



                  <h4>Rajesh Sinha



                  </h4>



                  <span>Business Analysis Professional



                  </span>



                </div>



                <div class="center mt-3">



                  <a href="https://abapro.skillsgroom.com/" class="button button-3d button-mini button-rounded button-aqua button-light">Reach Me



                  </a>



                </div>



              </div>



              <!--<div class="oc-item">-->



              <!--	<a href="https://www.bulletproofyourstartup.com/"><img src="http://staging.skillsgroom.com/UI/images/trainers/trainer-11.jpg" alt="Image 7" class="cropped"  title="Click to view"></a>-->



              <!--	<div class="member member-content">-->



              <!--          <h4>Ravi Challu</h4>-->



              <!--          <span>Startups Coach &amp; Mentor</span>-->



              <!--	</div>-->



              <!--	<div class="center mt-3"><a href="https://www.bulletproofyourstartup.com/" class="button button-3d button-mini button-rounded button-aqua button-light">Reach Me</a></div>-->



              <!--</div>-->



              <div class="oc-item">



                <a href="https://skillsgroom.com/sites/vikasgupta/">



                  <img src="https://staging.skillsgroom.com/UI/images/trainers/trainer-10.jpg" alt="Image 8" class="cropped" title="Click to view">



                </a>



                <div class="member member-content no-shadow">



                  <h4>Vikas Gupta



                  </h4>



                  <span>Guitar Teaching Specialist



                  </span>



                </div>



                <div class="center mt-3">



                  <a href="https://skillsgroom.com/sites/vikasgupta" class="button button-3d button-mini button-rounded button-aqua button-light">Reach Me



                  </a>



                </div>



              </div>



              <div class="oc-item">



                <a href="https://skillsgroom.com/sites/harishverma">



                  <img src="https://staging.skillsgroom.com/UI/images/trainers/male.jpg" alt="Image 9" class="cropped" title="Click to view">



                </a>



                <div class="member member-content no-shadow">



                  <h4>Harish Verma



                  </h4>



                  <span>Music Teacher



                  </span>



                </div>



                <div class="center mt-3">



                  <a href="https://skillsgroom.com/sites/harishverma" class="button button-3d button-mini button-rounded button-aqua button-light">Reach Me



                  </a>



                </div>



              </div>



            </div>



          </div>



        </div>



      </section> --}}



      <!-- End Trainers Section -->



      <!-- Promo Section



============================================= -->



      {{-- <div class="section m-0"



           style="z-index:0;padding: 120px 0; background: #FFF url('http://staging.skillsgroom.com/UI/images/instructor.jpg') no-repeat right top / cover">



        <div class="container">



          <div class="row">



            <div class="col-md-5">



            </div>



            <div class="col-md-7">



              <div class="heading-block nobottomborder mb-4 mt-5">



                <h3>Become a Coaching Partner!



                </h3>



                <span>Teach What You Love.



                </span>



              </div>



              <p class="mb-2">If you are passionate about teaching or already into an online teaching , join us today to



                increase your reach multifolds in the next 1 year.



              </p>



              <p>Be a part of our success story and make an impact through interactive online teaching .



              </p>



              <button class="button button-rounded button-xlarge ls0 ls0 nott t400 nomargin" onclick="openForm()">Inquiry



                email



              </button>



            </div>



          </div>



        </div>



      </div> --}}



    </div>



    <!-- #content end -->



    <!-- Footer



============================================= -->



    <footer id="footer" class="dark">

    <div class="container">

      <!-- Footer Widgets
              ============================================= -->
      <div class="footer-widgets-wrap clearfix">

        <div class="col_two_third">

          <div class="col_one_third">

            <div class="widget clearfix">

              <!--<h4>Locations</h4>-->
              <div style="background: url('images/world-map.png') no-repeat center center; background-size: 100%;">
                <address>
                  B305, Celebrity Square, Bidaraguppe<br>
                  Sarjapur Attibele Main Road,<br>
                  Bengaluru, Karnataka, India -562107.
                </address>
                <abbr title="Phone Number"><strong>Phone:</strong></abbr> +91 9916669702<br>
                <abbr title="Email Address"><strong>Email:</strong></abbr> <a href="mailto:info@skillsgroom.com">info@skillsgroom.com</a>
                <div class="widget clearfix">
                  <!--<h4>Follow Us</h4>-->
                  <a href="https://www.facebook.com/SkillsGroom" target="_blank" class="social-icon si-dark si-colored si-facebook nobottommargin"
                     style="margin-right: 10px;">
                    <i class="icon-facebook"></i>
                    <i class="icon-facebook"></i>
                  </a>
                  <!--<a href="#" class="social-icon si-dark si-colored si-twitter nobottommargin" style="margin-right: 10px;">
                                          <i class="icon-twitter"></i>
                                          <i class="icon-twitter"></i>
                                      </a>-->
                  <a href="https://www.instagram.com/skills_groom" target="_blank" class="social-icon si-dark si-colored si-instagram nobottommargin"
                     style="margin-right: 10px;">
                    <i class="icon-instagram"></i>
                    <i class="icon-instagram"></i>
                  </a>
                  <a href="https://www.youtube.com/channel/UCwhWbSkbUCn8_vG_06sj8cQ?view_as=subscriber" target="_blank" class="social-icon si-dark si-colored si-youtube nobottommargin"
                     style="margin-right: 10px;">
                    <i class="icon-youtube"></i>
                    <i class="icon-youtube"></i>
                  </a>
                </div>
              </div>

            </div>

          </div>
			<div class="col_one_third">

							<div class="widget widget_links clearfix">

								<ul>
									<li><a href="about.html">About Us</a></li>
									<li><a href="https://skillsgroom.com/sites/hobbyclasses/"><div>Music and Hobby classe</div></a></li>
								<li><a href="https://skillsgroom.com/sites/hobbyclasses/"><div>Chess and Sudoku classes</div></a></li>
								<li><a href="https://skillsgroom.com/sites/technicaltraining/"><div>Technical Trainings</div></a></li>

										</ul>


							</div>

						</div>
			<div class="col_one_third col_last">

							<div class="widget widget_links clearfix">

								<ul>

								<li><a href="coding_kids.html"><div>Coding for Kids</div></a></li>
								<li><a href="tuition.html"><div>Tuition</div></a></li>
								<li><a href="homeschooling.html"><div>Homeschooling</div></a></li>
								<li><a href="#"><div>Softskills and professional skills</div></a></li>
										</ul>


							</div>

						</div>
		  </div>

			<div class="col_one_third col_last">

							<div class="widget widget_links clearfix">

								<ul>
								<li><a href="solutions.html"><div>Solutions for School /Colleges</div></a></li>
										</ul>


							</div>

						</div>




      </div><!-- .footer-widgets-wrap end -->

    </div>

    <!-- Copyrights
          ============================================= -->
    <div id="copyrights">

      <div class="container clearfix">


        &copy; Copyright 2020 All Rights Reserved by <a href="index.php">Skillsgroom</a>
<br>
<div class="copyright-links"><a href="terms.php">Terms of Use</a> / <a href="privacy.php">Privacy Policy</a></div>

      </div>

    </div><!-- #copyrights end -->

  </footer>



    <!-- #footer end -->



    </div>



  <!-- #wrapper end -->



  <!-- Book Now -->



  <div id="BookNow" class="modal fade" role="dialog">



    <div class="modal-dialog">



      <!-- Book Now content-->



      <div class="modal-content">



        <div class="modal-header">



          <button type="button" class="close" data-dismiss="modal">&times;



          </button>



        </div>



        <div class="modal-body">



          <h3>Book Free Demo Classes



          </h3>



          <div class="alert alert-success" id="ShowMsg" style="display: none;">



            <strong id="SussMsg">



            </strong>



          </div>



          <form action="">



            <div class="form-group">



              <label for="">First Name



              </label>



              <input type="text" name="" id="first_name" class="form-control" placeholder="Enter Your First Name">



            </div>



            <div class="form-group">



              <label for="">Last Name



              </label>



              <input type="text" name="" id="last_name" class="form-control" placeholder="Enter Your Last Name">



            </div>



            <div class="form-group">



              <label for="">Email Id



              </label>



              <input type="text" name="" id="email" class="form-control" placeholder="Enter Your Email Id">



            </div>



            <div class="form-group">



              <label for="">Contact Number



              </label>



              <input type="text" name="" id="contact" class="form-control" placeholder="Enter Your Contact Number">



            </div>



            <div class="form-group">



              <label for="">Country



              </label>



              <input type="text" name="" id="country" class="form-control" placeholder="Enter Your Country ">



            </div>



            <div class="form-group">



              <label for="">State



              </label>



              <input type="text" name="" id="state" class="form-control" placeholder="Enter Your State">



            </div>



            <div class="form-group">



              <label for="">City



              </label>



              <input type="text" name="" id="city" class="form-control" placeholder="Enter your City">



            </div>



            <div class="form-group pull-right">



              <button type="button" id="BookClass" name="send" class="btn btn-success ml-2" value="Submit Details">Submit



              </button>



            </div>



          </form>



        </div>



      </div>



    </div>



  </div>



  <!-- Go To Top



============================================= -->



  <div id="gotoTop" class="icon-angle-up">



  </div>



  <!-- External JavaScripts



============================================= -->



  <script data-cfasync="false" src="/cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js">



  </script>



  <script src="{{URL::asset('UI/js/jquery.js')}}">



  </script>



  <script src="{{URL::asset('UI/js/plugins.js')}}">



  </script>







  <script src="{{URL::asset('UI/js/jquery.cslider.js')}}">



  </script>



  <script src="{{URL::asset('UI/js/datepicker.js')}}">



  </script>



  <!-- Footer Scripts



============================================= -->



  <script src="{{URL::asset('UI/js/functions.js')}}">



  </script>



  <script src="{{URL::asset('UI/js/custom/class.js')}}">



  </script>



  <script>



    function openForm() {



      document.getElementById("loginPopup").style.display = "block";



      document.getElementById("pop_up_overflow").style.overflow = "hidden";



    }



    function closeForm() {



      document.getElementById("loginPopup").style.display = "none";



      document.getElementById("pop_up_overflow").style.overflow = "initial";



    }



    // When the user clicks anywhere outside of the modal, close it



    window.onclick = function (event) {



      var modal = document.getElementById('loginPopup');



      if (event.target == modal) {



        closeForm();



      }



    }



    $('#cascade-slider').cascadeSlider({



      itemClass: 'cascade-slider_item',



      arrowClass: 'cascade-slider_arrow'



    }



                                      );



  </script>



  </body>



</html>



