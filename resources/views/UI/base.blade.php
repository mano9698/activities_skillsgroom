<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>

	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <?php
    $GetQuestions = json_decode(file_get_contents("https://onlinelms.skillsgroom.com/api/forum_list/1"), true);

   $Questions = $GetQuestions['data'];
   ?>

       <meta http-equiv="content-type" content="text/html; charset=utf-8"/>


       <meta name="author" content="Skillsgroom">
       <meta name="description" content="Skillsgroom is India's fastest growing online teaching solutions provider for Sudoku, Chess, Music and Extracurricular Activities. We are making online learning experience more effective and affordable.">
       <meta name="keywords" content="@foreach($Questions as $Title) {{ $Title['topic'] }}, @endforeach">

	<!-- Stylesheets
	============================================= -->
	 <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Ubuntu:300,300i,400,400i,500,500i,600,600i,700,700i,800,800i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Lato:300,400,400i,700|Raleway:300,400,500,600,700|Crete+Round:400i" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="{{URL::asset('UI/css/bootstrap.css')}}" type="text/css" />
	<link rel="stylesheet" href="{{URL::asset('UI/css/style.css')}}" type="text/css" />
	<link rel="stylesheet" href="{{URL::asset('UI/css/dark.css')}}" type="text/css" />
	<link rel="stylesheet" href="{{URL::asset('UI/css/font-icons.css')}}" type="text/css" />
	<link rel="stylesheet" href="{{URL::asset('UI/css/animate.css')}}" type="text/css" />
	<link rel="stylesheet" href="{{URL::asset('UI/css/magnific-popup.css')}}" type="text/css" />
	<!-- Date & Time Picker CSS -->
	<link rel="stylesheet" href="{{URL::asset('UI/css/datepicker.css')}}" type="text/css" />

	<link rel="stylesheet" href="{{URL::asset('UI/css/responsive.css')}}" type="text/css" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />

	<!-- Document Title
	============================================= -->
	<title>::Welcome to Skillsgroom | Forum::</title>

</head>

<body class="stretched">

	<!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="clearfix">

		<!-- Header
		============================================= -->
        @include('UI.common.header')
        <!-- #header end -->

		<!--<section id="slider" class="slider-element slider-parallax" style="background: none;" data-height-xl="50" data-height-lg="50" data-height-md="50" data-height-sm="50" data-height-xs="50">

		</section>-->


		<!-- Content
		============================================= -->
        @yield('Content')
        <!-- #content end -->

		<!-- Footer
		============================================= -->
		<footer id="footer" class="dark">

    <div class="container">

      <!-- Footer Widgets
              ============================================= -->
      <div class="footer-widgets-wrap clearfix">

        <div class="col_two_third">

          <div class="col_one_third">

            <div class="widget clearfix">

              <!--<h4>Locations</h4>-->
              <div style="background: url('images/world-map.png') no-repeat center center; background-size: 100%;">
                <address>
                  B305, Celebrity Square, Bidaraguppe<br>
                  Sarjapur Attibele Main Road,<br>
                  Bengaluru, Karnataka, India -562107.
                </address>
                <abbr title="Phone Number"><strong>Phone:</strong></abbr> +91 9916669702<br>
                <abbr title="Email Address"><strong>Email:</strong></abbr> <a href="mailto:info@skillsgroom.com">info@skillsgroom.com</a>
                <div class="widget clearfix">
                  <!--<h4>Follow Us</h4>-->
                  <a href="https://www.facebook.com/SkillsGroom" target="_blank" class="social-icon si-dark si-colored si-facebook nobottommargin"
                     style="margin-right: 10px;">
                    <i class="icon-facebook"></i>
                    <i class="icon-facebook"></i>
                  </a>
                  <!--<a href="#" class="social-icon si-dark si-colored si-twitter nobottommargin" style="margin-right: 10px;">
                                          <i class="icon-twitter"></i>
                                          <i class="icon-twitter"></i>
                                      </a>-->
                  <a href="https://www.instagram.com/skills_groom" target="_blank" class="social-icon si-dark si-colored si-instagram nobottommargin"
                     style="margin-right: 10px;">
                    <i class="icon-instagram"></i>
                    <i class="icon-instagram"></i>
                  </a>
                  <a href="https://www.youtube.com/channel/UCwhWbSkbUCn8_vG_06sj8cQ?view_as=subscriber" target="_blank" class="social-icon si-dark si-colored si-youtube nobottommargin"
                     style="margin-right: 10px;">
                    <i class="icon-youtube"></i>
                    <i class="icon-youtube"></i>
                  </a>
                </div>
              </div>

            </div>

          </div>

        </div>


      </div><!-- .footer-widgets-wrap end -->

    </div>

    <!-- Copyrights
          ============================================= -->
    <div id="copyrights">

      <div class="container clearfix">


        © Copyright 2020 All Rights Reserved by <a href="/">Skillsgroom</a>


      </div>

    </div><!-- #copyrights end -->

  </footer><!-- #footer end -->

	</div><!-- #wrapper end -->

	<!-- Go To Top
	============================================= -->
	<div id="gotoTop" class="icon-angle-up"></div>

	<!-- External JavaScripts
	============================================= -->
	<script src="{{URL::asset('UI/js/jquery.js')}}"></script>
	<script src="{{URL::asset('UI/js/plugins.js')}}"></script>

  <script src="{{URL::asset('UI/js/custom/class.js')}}">

	<script src="{{URL::asset('UI/js/datepicker.js')}}"></script>

	<!-- Footer Scripts
	============================================= -->
	<script src="{{URL::asset('UI/js/functions.js')}}"></script>




</body>
</html>
