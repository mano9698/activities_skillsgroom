<!DOCTYPE html>

<html dir="ltr" lang="en-US">

<head>



	<meta http-equiv="content-type" content="text/html; charset=utf-8" />

	<meta name="author" content="SemiColonWeb" />



	<!-- Stylesheets

	============================================= -->

	 <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Ubuntu:300,300i,400,400i,500,500i,600,600i,700,700i,800,800i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

	<link href="https://fonts.googleapis.com/css?family=Lato:300,400,400i,700|Raleway:300,400,500,600,700|Crete+Round:400i" rel="stylesheet" type="text/css" />

	<link rel="stylesheet" href="{{URL::asset('UI/css/font-awesome.css')}}">

    <link rel="stylesheet" href="{{URL::asset('UI/css/bootstrap.css')}}" type="text/css"/>

    <link rel="stylesheet" href="{{URL::asset('UI/css/style.css')}}" type="text/css"/>

    <link rel="stylesheet" href="{{URL::asset('UI/css/style2.css')}}" type="text/css"/>

    <link rel="stylesheet" href="{{URL::asset('UI/css/dark.css')}}" type="text/css"/>

    <link rel="stylesheet" href="{{URL::asset('UI/css/font-icons.css')}}" type="text/css"/>

    <link rel="stylesheet" href="{{URL::asset('UI/css/animate.css')}}" type="text/css"/>

    <link rel="stylesheet" href="{{URL::asset('UI/css/magnific-popup.css')}}" type="text/css"/>

    <!-- Date & Time Picker CSS -->

    <link rel="stylesheet" href="{{URL::asset('UI/css/datepicker.css')}}" type="text/css"/>

    <!-- Hosting Demo Specific Stylesheet -->

    <link rel="stylesheet" href="{{URL::asset('UI/css/course.css')}}" type="text/css"/>

    <link rel="stylesheet" href="{{URL::asset('UI/css/responsive.css')}}" type="text/css"/>

	<meta name="viewport" content="width=device-width, initial-scale=1" />



	<!-- Document Title

	============================================= -->

	<title>::Welcome to Skillsgroom | Events Details::</title>



</head>



<body class="stretched">



	<!-- Document Wrapper

	============================================= -->

	<div id="wrapper" class="clearfix">



		<!-- Header

		============================================= -->

		<header id="header" class="transparent-header dark full-header" data-sticky-class="not-dark">



			<div id="header-wrap">



				<div class="container clearfix">



					<!--<div id="primary-menu-trigger"><i class="icon-reorder"></i></div>-->



					<!-- Logo

					============================================= -->

                    <div id="logo">

                        <a href="/index.php" class="standard-logo" data-dark-logo="{{URL::asset('UI/images/logo-dark.png')}}">

                          <img src="{{URL::asset('UI/images/logo.png')}}" alt="Skillsgroom">

                        </a>

                        <a href="/index.php" class="retina-logo" data-dark-logo="{{URL::asset('UI/images/logo-dark@2x.png')}}">

                          <img src="{{URL::asset('UI/images/logo-dark.png')}}" alt="Skillsgroom">

                        </a>

                      </div>



					<!-- Primary Navigation

					============================================= -->

					<nav id="primary-menu">



						<ul>

							<!--<li><a href="index.html"><div>Home</div></a></li>

							<li><a href="about.html"><div>About Us</div></a></li>

							<li><a href="contact.html"><div>Contact Us</div></a></li>-->



						</ul>



					</nav><!-- #primary-menu end -->



				</div>



			</div>



		</header><!-- #header end -->



		<section id="slider" class="slider-element slider-parallax mobile_forum" style="background: url('{{URL::asset('UI/images/inner_banner.jpg')}}') no-repeat; background-size: cover">



		</section>





		<!-- Content

		============================================= -->

		<div id="content">

<div class="content-wrap">



<div class="container clearfix">

	<div id="section-price" class="section mt-0 pt-0" style="background: none;">

					<div class="container">

						<div class="row justify-content-between">

							<div class="col-lg-4 col-md-5 mt-4 mt-md-0 mr-0 sticky-sidebar-wrap sidebar">



						<div class="card events-meta mb-3">

							<div class="image">

								@if($Courses['course_image'])

									<img  src="https://onlinelms.skillsgroom.com/UI/courses/{{$Courses['course_image']}}" alt="Card image cap" />

								@else

									<img  src="https://onlinelms.skillsgroom.com/UI/images/1.jpg" alt="Card image cap" />

								@endif



							</div>

									<div class="card-header"><h5 class="mb-0">Classes Details</h5></div>

									<div class="card-body">

										<ul class="iconlist nobottommargin">

                                            <li><i class="icon-calendar3"></i>
                                                @if(isset($Course['course_date']))
                                                    {{date('d M Y', strtotime($Courses['course_date']))}}
                                                @else

                                                @endif</li>

											<li><i class="icon-time"></i> {{$Courses['course_time']}}</li>

											<li><i class="icon-map-marker2"></i> {{$Courses['location']}}</li>

											<li><i class="icon-rupee"></i> <strong>{{$Courses['price']}}</strong></li>

										</ul>



										{{-- <h4>Key Benefits</h4>

								<ol class="keylist nobottommargin"><li>Over 37 lectures and 55.5 hours of content!</li>

									<li>Testing Training Included.</li>

									<li>Best suitable for beginners to advanced users.</li>

									<li>Course content designed job market.</li>

									<li>Practical assignments at the end .</li>

									<li>Unlimited Resourses.</li>

									<li>Practical learning experience with live project .</li></ol> --}}

</div>

								</div>



								<a href="/book_demo/{{$Courses['id']}}/{{$Courses['UserId']}}" class="btn btn-success btn-block btn-lg" target="_blank">Book a demo class</a>





							</div>

							<div class="col-lg-8 postcontent nobottommargin col_last clearfix">





						<div class="single-event">



							<div class="section-title">

          <h2>Courses</h2>

							<p class="event_head">{{$Courses['title']}}</p>

        </div>



								<h3>Course Description</h3>



								{!!$Courses['course_description']!!}



							<h3>Instructor</h3>

							<div class="card">

								<div class="card-body">

<div class="spost clearfix">

										<div class="entry-image">

											<a href="#" class="nobg">

												@if($Courses['profile_pic'])

									<img src="https://onlinelms.skillsgroom.com/UI/teachers_profile_pic/{{$Courses['profile_pic']}}" alt="">

								@else

									<img  src="https://onlinelms.skillsgroom.com/UI/user.jpg" alt="Card image cap">

								@endif



											</a>

										</div>

										<div class="entry-c">

											<div class="entry-title">

												<h4><div class="entry-social">

	{{-- <div class="social">


				  @if($Courses['facebook_link'])

					  <a href="{{$Courses['facebook_link']}}"><i class="icon-facebook"></i></a>

					@else`

						<a href="https://www.facebook.com/SkillsGroom"><i class="icon-facebook"></i></a>

					@endif



					@if($Courses['instagram_link'])

				 	 <a href="{{$Courses['instagram_link']}}"><i class="icon-instagram"></i></a>



				 	 @else`

					  <a href="https://www.instagram.com/skills_groom/"><i class="icon-instagram"></i></a>

					@endif



					@if($Courses['youtube_channel'])

				 	 	<a href="{{$Courses['youtube_channel']}}"><i class="icon-youtube"></i></a>

				 	 @else

					  	<a href=" https://www.youtube.com/channel/UCwhWbSkbUCn8_vG_06sj8cQ?view_as=subscriber"><i class="icon-youtube"></i></a>

				 	 @endif

                </div> --}}

	</div>{{$Courses['first_name']}} {{$Courses['last_name']}}</h4>
    <br>
												{{-- <i class="entry-profile">Entrepreneur and Accounting Coach</i> --}}

											</div>



											{!!$Courses['about_me']!!}



										</div>



									</div>

								</div></div>

							<div class="clear"></div>



						</div>





							</div>



						</div>

					</div>

				</div>





				</div>

			</div>

		</div><!-- #content end -->



		<!-- Footer

		============================================= -->

		<footer id="footer" class="dark">

    <div class="container">

      <!-- Footer Widgets
              ============================================= -->
      <div class="footer-widgets-wrap clearfix">

        <div class="col_two_third">

          <div class="col_one_third">

            <div class="widget clearfix">

              <!--<h4>Locations</h4>-->
              <div style="background: url('images/world-map.png') no-repeat center center; background-size: 100%;">
                <address>
                  B305, Celebrity Square, Bidaraguppe<br>
                  Sarjapur Attibele Main Road,<br>
                  Bengaluru, Karnataka, India -562107.
                </address>
                <abbr title="Phone Number"><strong>Phone:</strong></abbr> +91 9916669702<br>
                <abbr title="Email Address"><strong>Email:</strong></abbr> <a href="mailto:info@skillsgroom.com">info@skillsgroom.com</a>
                <div class="widget clearfix">
                  <!--<h4>Follow Us</h4>-->
                  <a href="https://www.facebook.com/SkillsGroom" target="_blank" class="social-icon si-dark si-colored si-facebook nobottommargin"
                     style="margin-right: 10px;">
                    <i class="icon-facebook"></i>
                    <i class="icon-facebook"></i>
                  </a>
                  <!--<a href="#" class="social-icon si-dark si-colored si-twitter nobottommargin" style="margin-right: 10px;">
                                          <i class="icon-twitter"></i>
                                          <i class="icon-twitter"></i>
                                      </a>-->
                  <a href="https://www.instagram.com/skills_groom" target="_blank" class="social-icon si-dark si-colored si-instagram nobottommargin"
                     style="margin-right: 10px;">
                    <i class="icon-instagram"></i>
                    <i class="icon-instagram"></i>
                  </a>
                  <a href="https://www.youtube.com/channel/UCwhWbSkbUCn8_vG_06sj8cQ?view_as=subscriber" target="_blank" class="social-icon si-dark si-colored si-youtube nobottommargin"
                     style="margin-right: 10px;">
                    <i class="icon-youtube"></i>
                    <i class="icon-youtube"></i>
                  </a>
                </div>
              </div>

            </div>

          </div>
			<div class="col_one_third">

							<div class="widget widget_links clearfix">

								<ul>
									<li><a href="about.html">About Us</a></li>
									<li><a href="https://skillsgroom.com/sites/hobbyclasses/"><div>Music and Hobby classe</div></a></li>
								<li><a href="https://skillsgroom.com/sites/hobbyclasses/"><div>Chess and Sudoku classes</div></a></li>
								<li><a href="https://skillsgroom.com/sites/technicaltraining/"><div>Technical Trainings</div></a></li>

										</ul>


							</div>

						</div>
			<div class="col_one_third col_last">

							<div class="widget widget_links clearfix">

								<ul>

								<li><a href="coding_kids.html"><div>Coding for Kids</div></a></li>
								<li><a href="tuition.html"><div>Tuition</div></a></li>
								<li><a href="homeschooling.html"><div>Homeschooling</div></a></li>
								<li><a href="#"><div>Softskills and professional skills</div></a></li>
										</ul>


							</div>

						</div>
		  </div>

			<div class="col_one_third col_last">

							<div class="widget widget_links clearfix">

								<ul>
								<li><a href="solutions.html"><div>Solutions for School /Colleges</div></a></li>
										</ul>


							</div>

						</div>




      </div><!-- .footer-widgets-wrap end -->

    </div>

    <!-- Copyrights
          ============================================= -->
    <div id="copyrights">

      <div class="container clearfix">


        &copy; Copyright 2020 All Rights Reserved by <a href="index.php">Skillsgroom</a>
<br>
<div class="copyright-links"><a href="terms.php">Terms of Use</a> / <a href="privacy.php">Privacy Policy</a></div>

      </div>

    </div><!-- #copyrights end -->

  </footer><!-- #footer end -->



	</div><!-- #wrapper end -->



	<!-- Go To Top

	============================================= -->

	<div id="gotoTop" class="icon-angle-up"></div>



	<!-- External JavaScripts

	============================================= -->

	<script data-cfasync="false" src="/cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js">

    </script>

    <script src="{{URL::asset('UI/js/jquery.js')}}">

    </script>

    <script src="{{URL::asset('UI/js/plugins.js')}}">

    </script>



    <script src="{{URL::asset('UI/js/jquery.cslider.js')}}">

    </script>

    <script src="{{URL::asset('UI/js/datepicker.js')}}">

    </script>

    <!-- Footer Scripts

  ============================================= -->

    <script src="{{URL::asset('UI/js/functions.js')}}">

    </script>

    <script src="{{URL::asset('UI/js/custom/class.js')}}">

    </script>









</body>

</html>
