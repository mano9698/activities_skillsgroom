
<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>

	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="author" content="SemiColonWeb" />

	<!-- Stylesheets
	============================================= -->
	<link rel="icon" href="images/favicon.ico" type="image/x-icon"/>
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon"/>
	 <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Ubuntu:300,300i,400,400i,500,500i,600,600i,700,700i,800,800i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Lato:300,400,400i,700|Raleway:300,400,500,600,700|Crete+Round:400i" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="{{URL::asset('UI/css/font-awesome.css')}}">
    <link rel="stylesheet" href="{{URL::asset('UI/css/bootstrap.css')}}" type="text/css"/>
    <link rel="stylesheet" href="{{URL::asset('UI/css/style.css')}}" type="text/css"/>
    <link rel="stylesheet" href="{{URL::asset('UI/css/dark.css')}}" type="text/css"/>
    <link rel="stylesheet" href="{{URL::asset('UI/css/font-icons.css')}}" type="text/css"/>
    <link rel="stylesheet" href="{{URL::asset('UI/css/animate.css')}}" type="text/css"/>
    <link rel="stylesheet" href="{{URL::asset('UI/css/magnific-popup.css')}}" type="text/css"/>
    <!-- Date & Time Picker CSS -->
    <link rel="stylesheet" href="{{URL::asset('UI/css/datepicker.css')}}" type="text/css"/>

    <!-- Hosting Demo Specific Stylesheet -->
    <link rel="stylesheet" href="{{URL::asset('UI/css/course.css')}}" type="text/css"/>

    <link rel="stylesheet" href="{{URL::asset('UI/css/responsive.css')}}" type="text/css"/>
	<meta name="viewport" content="width=device-width, initial-scale=1" />

	<!-- Document Title
	============================================= -->
	<title>::Welcome to Skillsgroom | Trainer Details::</title>

</head>

<body class="stretched">

	<!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="clearfix">

		<!-- Header
		============================================= -->
		<header id="header" class="transparent-header dark full-header" data-sticky-class="not-dark">

			<div id="header-wrap">

				<div class="container clearfix">

					<div id="primary-menu-trigger"><i class="icon-reorder"></i></div>

					<!-- Logo
					============================================= -->
					<div id="logo">
						<a href="index.html" class="standard-logo" data-dark-logo="{{URL::asset('UI/images/logo-dark.png')}}"><img src="{{URL::asset('UI/images/logo.png')}}" alt="Canvas Logo"></a>
						<a href="index.html" class="retina-logo" data-dark-logo="{{URL::asset('UI/images/logo-dark@2x.png')}}"><img src="{{URL::asset('UI/images/logo@2x.png')}}" alt="Canvas Logo"></a>
					</div><!-- #logo end -->

					<!-- Primary Navigation
					============================================= -->
					<nav id="primary-menu">

						<ul>
							<li><a href="/index.php"><div>Home</div></a></li>
							<!--<li><a href="about.html"><div>About Us</div></a></li>
							<li><a href="contact.html"><div>Contact Us</div></a></li>-->

						</ul>

					</nav><!-- #primary-menu end -->

				</div>

			</div>

		</header><!-- #header end -->

		<section id="slider" class="slider-element slider-parallax" style="background: url('{{URL::asset('UI/images/inner_banner.jpg')}}') no-repeat; background-size: cover" data-height-xl="200" data-height-lg="200" data-height-md="200" data-height-sm="100" data-height-xs="100">

		</section>


		<!-- Content
		============================================= -->
		<div id="content">
<div class="content-wrap">

			<div class="container clearfix">
			<div class="section-title">
					  <h2>Instructor</h2>
					  {{-- <p>Advisor Details</p> --}}
					</div>
				<div class="sidebar nobottommargin clearfix" style="padding-right: 4%;">
						<div class="card events-meta mb-3">
							<div class="image">
                                <img src="https://onlinelms.skillsgroom.com/UI/teachers_profile_pic/{{ $Teachers['profile_pic'] }}" alt="" class="cropped_trainer" style="height:284px;"/></div>

									<div class="card-titlecontent">
										<h5>{{ $Teachers['first_name'] }} {{ $Teachers['last_name'] }}</h5>
										<div>{{ $Teachers['teaching_subjects'] }}</div>
										<div><i>{{ $Teachers['state'] }},{{ $Teachers['city'] }},{{ $Teachers['area'] }}</i></div>

										{{-- <div class="social trainer_social">
										  <a href="{{ $Teachers['website_link'] }}"><i class="icon-globe"></i></a>
										  <a href="{{ $Teachers['facebook_link'] }}"><i class="icon-facebook"></i></a>
										  <a href="{{ $Teachers['instagram_link'] }}"><i class="icon-instagram"></i></a>
										  <a href="{{ $Teachers['youtube_channel'] }}"><i class="icon-youtube"></i></a>
										</div> --}}


										</div>
						</div>



					</div>
					<div class="postcontent nobottommargin col_last clearfix" style="width:70%;">

						<div class="single-event">
						<!--<div class="course-info-list">
                            <ul>
                                <li>
                                    <h2>15</h2>
                                    <h5>Courses Authored</h5>
                                </li>
                                <li>
                                    <h2>120+</h2>
                                    <h5>Student Enrolled</h5>
                                </li>
                                <li>
                                    <h2>4.5</h2>
                                    <h5>Avg Ratings</h5>
                                </li>
                            </ul>
                        </div>-->
						<div class="tabs clearfix" id="tab-3">

							<ul class="tab-nav tab-nav2 clearfix">
								<li><a href="#tabs-1">Profile Summary</a></li>
								<li><a href="#tabs-2">Courses</a></li>
								<li><a href="#tabs-3">Videos</a></li>
							</ul>

							<div class="tab-container">

								<div class="tab-content clearfix" id="tabs-1">
									<table class="table">
									<tbody>
										<tr>
											<td class="notopborder" width="20%"><strong>About Me:</strong></td>
											<td class="notopborder">{!! $Teachers['about_me'] !!}</td>
										</tr>
										<tr>
											<td><strong>Gender:</strong></td>
											<td>{{ $Teachers['gender'] }}</td>
										</tr>
										<!--<tr>
											<td><strong>Contact:</strong></td>
											<td>9876543210</td>
										</tr>-->
										<tr>
											<td><strong>Qualification:</strong></td>
											<td>{{ $Teachers['qualification'] }}</td>
										</tr>
										<tr>
											<td><strong>Tutor Type:</strong></td>
											<td>{{ $Teachers['tutor_type'] }}</td>
										</tr>
										<tr>
											<td><strong>Teaching Subject:</strong></td>
											<td>{{ $Teachers['teaching_subjects'] }}</td>
										</tr>
										<tr>
											<td><strong>Teaching Language:</strong></td>
											<td>{{ $Teachers['teaching_language'] }}</td>
										</tr>

									</tbody>
								</table>
								</div>
								<div class="tab-content clearfix courses" id="tabs-2" style="background: none;">
									<!--Schedule Carousel-->
					<div id="oc-posts" class="owl-carousel posts-carousel carousel-widget" data-margin="20" data-nav="true" data-pagi="false" data-items-xs="1" data-items-sm="2" data-items-md="3" data-items-lg="4">
<!--Schedule Item 1-->
                        @if($Courses)
                        @foreach($Courses as $Course)
                            <div class="oc-item">
                                <div class="ipost clearfix">

                <div class="col-md-12 px-0">
                                    <div class="card course-card hover-effect noborder">
                                        <a href="/course_details/{{$Course['id']}}" target="_blank"><img class="card-img-top" src="https://onlinelms.skillsgroom.com/UI/courses/{{ $Course['course_image'] }}" alt="Card image cap"></a>
                                        <div class="card-body">
                                            <h4 class="card-title t700 mb-2"><a href="/course_details/{{$Course['id']}}" target="_blank">{{ $Course['title'] }}</a></h4>
                                            <p class="mb-2 card-title-sub uppercase t400 ls1"><a href="#" class="text-black-50">Category - {{ $Course['category'] }}</a></p>

                                            <p class="card-text text-black-50 mb-1">{!! $Course['course_description'] !!}</p>
                                        </div>
                                        <div class="card-footer py-3 d-flex justify-content-between align-items-center bg-white text-muted">
                                            <div class="badge alert-primary">Rs.{{ $Course['price'] }}</div>
                                            <div class="badge alert-warning">Duration : {{ $Course['duration'] }}</div>
                                        </div>
                                    </div>
                                </div>

                                </div>
                            </div>
                        @endforeach
                        @endif
<!--Schedule Item 2-->

					</div>
<!--Schedule Ends-->
								</div>

								<div class="tab-content clearfix" id="tabs-3">
									<!--<iframe frameborder="0" width="460" height="315" src="https://www.youtube.com/embed/jFKt6h2eenA" allowfullscreen></iframe>-->
									<div id="oc-posts1" class="owl-carousel posts-carousel carousel-widget" data-margin="20" data-nav="true" data-pagi="false" data-items-xs="1" data-items-sm="2" data-items-md="3" data-items-lg="4">

                        @if($Videos)
                            @foreach ($Videos as $Video)
                                <div class="oc-item">
                                    <div class="ipost clearfix">
                                        <div class="entry-image mb-2">
                                            <iframe width="560" height="315" src="{{ $Video['file'] }}" frameborder="0" allowfullscreen></iframe>
                                        </div>
                                        {{-- <div class="entry-title">
                                            <h6>The Name of the Video</h6>
                                        </div> --}}


                                    </div>
                                </div>
                            @endforeach

                        @endif

					</div>

								</div>

							</div>

						</div>

							<div class="clear"></div>
<h3>Testimonials</h3>

							<div id="oc-testi" class="owl-carousel testimonials-carousel carousel-widget" data-margin="20" data-items-sm="1" data-items-md="2" data-items-xl="3">

                        @if($Testimonials)
                            @foreach($Testimonials as $Testimonial)
                                <div class="oc-item">
                                    <div class="testimonial">

                                        <div class="testi-content">
                                            <p>
                                                <!--<i class="icon-quote-left"></i>-->
                                                {{ $Testimonial['testimonial'] }}
                                            <!--<i class="icon-quote-right"></i>-->
                                            </p>
                                            <div class="testi-image">
                                            <a href="#"><img src="https://onlinelms.skillsgroom.com/UI/students_profile_pic/{{ $Testimonial['profile_pic'] }}" alt="Customer Testimonails"></a>
                                        </div>
                                            <div class="testi-meta">
                                                {{ $Testimonial['first_name'] }} {{ $Testimonial['last_name'] }}
                                                <span>XYZ Inc.</span>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        @endif


					</div>
						</div>

					</div>



				</div>
			</div>
		</div><!-- #content end -->

		<!-- Footer
		============================================= -->
		<footer id="footer" class="dark">

    <div class="container">

      <!-- Footer Widgets
              ============================================= -->
      <div class="footer-widgets-wrap clearfix">

        <div class="col_two_third">

          <div class="col_one_third">

            <div class="widget clearfix">

              <!--<h4>Locations</h4>-->
              <div style="background: url('images/world-map.png') no-repeat center center; background-size: 100%;">
                <address>
                  B305, Celebrity Square, Bidaraguppe<br>
                  Sarjapur Attibele Main Road,<br>
                  Bengaluru, Karnataka, India -562107.
                </address>
                <abbr title="Phone Number"><strong>Phone:</strong></abbr> +91 9916669702<br>
                <abbr title="Email Address"><strong>Email:</strong></abbr> <a href="/cdn-cgi/l/email-protection#a3cacdc5cce3d0c8cacfcfd0c4d1ccccce8dc0ccce"><span class="__cf_email__" data-cfemail="7e171018113e0d151712120d190c111113501d1113">[email&#160;protected]</span></a>
                <div class="widget clearfix">
                  <!--<h4>Follow Us</h4>-->
                  <a href="https://www.facebook.com/SkillsGroom" target="_blank" class="social-icon si-dark si-colored si-facebook nobottommargin"
                     style="margin-right: 10px;">
                    <i class="icon-facebook"></i>
                    <i class="icon-facebook"></i>
                  </a>
                  <!--<a href="#" class="social-icon si-dark si-colored si-twitter nobottommargin" style="margin-right: 10px;">
                                          <i class="icon-twitter"></i>
                                          <i class="icon-twitter"></i>
                                      </a>-->
                  <a href="https://www.instagram.com/skills_groom" target="_blank" class="social-icon si-dark si-colored si-instagram nobottommargin"
                     style="margin-right: 10px;">
                    <i class="icon-instagram"></i>
                    <i class="icon-instagram"></i>
                  </a>
                  <a href="https://www.youtube.com/channel/UCwhWbSkbUCn8_vG_06sj8cQ?view_as=subscriber" target="_blank" class="social-icon si-dark si-colored si-youtube nobottommargin"
                     style="margin-right: 10px;">
                    <i class="icon-youtube"></i>
                    <i class="icon-youtube"></i>
                  </a>
                </div>
              </div>

            </div>

          </div>
			<div class="col_one_third">

							<div class="widget widget_links clearfix">

								<ul>
									<li><a href="about.html">About Us</a></li>
									<li><a href="https://skillsgroom.com/hobbyclasses/"><div>Music and Hobby classe</div></a></li>
								<li><a href="coding_kids.html"><div>Coding for Kids</div></a></li>
								<li><a href="tuition.html"><div>Tuition</div></a></li>
										</ul>


							</div>

						</div>
			<div class="col_one_third col_last">

							<div class="widget widget_links clearfix">

								<ul>

								<li><a href="homeschooling.html"><div>Homeschooling</div></a></li>
								<li><a href="https://skillsgroom.com/hobbyclasses/"><div>Chess and Sudoku classes</div></a></li>
								<li><a href="https://skillsgroom.com/technicaltraining/"><div>Technical Trainings</div></a></li>
								<li><a href="#"><div>Softskills and professional skills</div></a></li>					<!--<li><a href="solutions.html"><div>Solutions for School /Colleges</div></a></li>-->
										</ul>


							</div>

						</div>

        </div>
<div class="col_one_third col_last" style="width: 25%;">
		  <div class="widget widget_links clearfix">

								<ul>
									<li><a href="solutions.html"><div>Solutions for School /Colleges</div></a></li>
										</ul>


							</div>
		  </div>

      </div><!-- .footer-widgets-wrap end -->

    </div>

    <!-- Copyrights
          ============================================= -->
    <div id="copyrights">

      <div class="container clearfix">


        &copy; Copyright 2020 All Rights Reserved by <a href="index.php">Skillsgroom</a>
<br>
<div class="copyright-links"><a href="terms.php">Terms of Use</a> / <a href="privacy.php">Privacy Policy</a></div>

      </div>

    </div><!-- #copyrights end -->

  </footer><!-- #footer end -->

	</div><!-- #wrapper end -->

	<!-- Go To Top
	============================================= -->
	<div id="gotoTop" class="icon-angle-up"></div>

	<!-- External JavaScripts
	============================================= -->
	<script src="{{URL::asset('UI/js/jquery.js')}}"></script>
<script src="{{URL::asset('UI/js/plugins.js')}}"></script>


<script src="{{URL::asset('UI/js/datepicker.js')}}"></script>

<!-- Footer Scripts
  ============================================= -->
<script src="{{URL::asset('UI/js/functions.js')}}"></script>

    <script src="{{URL::asset('UI/js/custom/class.js')}}"></script>



</body>
</html>
