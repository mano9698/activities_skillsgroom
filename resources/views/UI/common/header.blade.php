<header id="header" class="transparent-header dark full-header" data-sticky-class="not-dark">

    <div id="header-wrap">

        <div class="container clearfix">

            <div id="primary-menu-trigger"><i class="icon-reorder"></i></div>

            <!-- Logo
            ============================================= -->
            <div id="logo" style="border: 0;">
                <a href="/index.php" class="standard-logo" data-dark-logo="{{URL::asset('UI/images/logo-dark.png')}}">

                    <img src="{{URL::asset('UI/images/logo.png')}}" alt="Skillsgroom">

                  </a>

                  <a href="/index.php" class="retina-logo" data-dark-logo="{{URL::asset('UI/images/logo-dark@2x.png')}}">

                    <img src="{{URL::asset('UI/images/logo-dark.png')}}" alt="Skillsgroom">

                  </a>
            </div><!-- #logo end -->

            <!-- Primary Navigation
            ============================================= -->
            <nav id="primary-menu">

                <ul style="border: 0;">
                    <li><a href="/"><div style="color: #F26222;">Home</div></a></li>
                    <!--<li><a href="about.html"><div>About Us</div></a></li>
                    <li><a href="contact.html"><div>Contact Us</div></a></li>-->

                </ul>

            </nav><!-- #primary-menu end -->

        </div>

    </div>

</header>
