@extends('UI.base')
@section('Content')
<div id="content">
    <div class="forum_header"></div>
<div class="content-wrap">

<div class="container clearfix">
<div id="section-price" class="section my-0 py-0" style="background: none;">
            <div class="container">
                <div class="row justify-content-between">
                    <div class="col-lg-4 col-md-5 mt-4 mt-md-0 mr-0 sticky-sidebar-wrap sidebar">

                <div class="card events-meta mb-3">


                            <div class="card-body topics_bg">


                                <h4>More Topics</h4>
                        <ol class="keylist nobottommargin" style="line-height: 32px;">
                            @if($Questions)
                              <li>
                                <a href="/forum/details/{{$Questions['id']}}/{{$Questions['slug']}}">{{$Questions['topic']}}
                                </a>
                              </li>
                            @else
                              <h4>No data found</h4>
                            @endif


                                </ol>
</div>
                        </div>

                        <!--<div class="image pt-5"><img src="images/forum/ad_forum.jpg" alt=""/></div>-->
                {{-- <div class="col-10 widget clearfix">

                        <h4>Tag Questions</h4>
                        <div class="tagcloud">
                            <a href="#">general</a>
                            <a href="#">videos</a>
                            <a href="#">music</a>
                            <a href="#">media</a>
                            <a href="#">photography</a>
                            <a href="#">parallax</a>
                            <a href="#">ecommerce</a>
                            <a href="#">terms</a>
                            <a href="#">coupons</a>
                            <a href="#">modern</a>
                        </div>

                    </div> --}}

                    </div>
                    <div class="col-lg-8 postcontent nobottommargin col_last clearfix">


                <div class="single-post nobottommargin">

                    <!-- Single Post
                    ============================================= -->
                    <div class="entry clearfix">

                        <!-- Entry Title
                        ============================================= -->
                        <div class="entry-title">
                            <h2>{{$Questions['topic']}}</h2>
                        </div><!-- .entry-title end -->

                        <!-- Entry Meta
                        ============================================= -->
                        <ul class="entry-meta clearfix">
                            <li><i class="icon-calendar3"></i>{{date('M d Y', strtotime($Questions['created_at']))}} at {{date('h:i a', strtotime($Questions['created_at']))}}</li>
                            <li><a href="#"><i class="icon-user"></i> {{$Questions['first_name']}} {{$Questions['last_name']}}</a></li>
                            <li><i class="icon-folder-open"></i> <a href="#">{{$Questions['subject']}}</a></li>
                            <li><a href="#"><i class="icon-comments"></i> {{count($Comments)}} Comments</a></li>
                            <li><a href="#"><i class="icon-like"></i> {{count($Likes)}}  Likes</a></li>
                        </ul><!-- .entry-meta end -->

                        <!-- Entry Image
                        ============================================= -->
                        <div class="entry-image">
                            <a href="#">
                                @if($Questions['feature_img'])
                                    <img src="https://onlinelms.skillsgroom.com/UI/questions/{{$Questions['feature_img']}}" alt="">
                                @else

                                @endif
                            </a>
                        </div><!-- .entry-image end -->

                        <!-- Entry Content
                        ============================================= -->
                        <div class="entry-content notopmargin">

                            <p>{!!$Questions['details_answer']!!}</p>


                            <!-- Post Single - Content End -->

                            <!-- Tag Cloud
                            ============================================= -->
                            {{-- <div class="tagcloud clearfix bottommargin">
                                <a href="#">general</a>
                                <a href="#">information</a>
                                <a href="#">media</a>
                                <a href="#">press</a>
                                <a href="#">gallery</a>
                                <a href="#">illustration</a>
                            </div> --}}
                            <!-- .tagcloud end -->

                            <div class="clear"></div>

                            <!-- Post Single - Share
                            ============================================= -->
                            <div class="si-share noborder clearfix">
                                <b style="float:left; line-height: 20px;">Like this Post:<br>
                                <a href="javascript:void(0);" id="AddLikes" class="social-icon si-borderless si-dribbble">
                                        <i class="icon-like"></i>
                                        <i class="icon-like"></i>
                                    </a>
                                </b>
                                <div style="font-weight: bold;">Share this Post:<br>
                                    <a href="#" class="social-icon si-borderless si-facebook">
                                        <i class="icon-facebook"></i>
                                        <i class="icon-facebook"></i>
                                    </a>
                                    <a href="#" class="social-icon si-borderless si-twitter">
                                        <i class="icon-twitter"></i>
                                        <i class="icon-twitter"></i>
                                    </a>
                                    <a href="#" class="social-icon si-borderless si-gplus">
                                        <i class="icon-gplus"></i>
                                        <i class="icon-gplus"></i>
                                    </a>
                                    <a href="#" class="social-icon si-borderless si-email3">
                                        <i class="icon-email3"></i>
                                        <i class="icon-email3"></i>
                                    </a>
                                </div>
                            </div><!-- Post Single - Share End -->

                        </div>
                    </div><!-- .entry end -->

                    <!-- Post Navigation
                    ============================================= -->
                    <!--<div class="post-navigation clearfix">

                        <div class="col_half nobottommargin">
                            <a href="#">&lArr; Previous Question</a>
                        </div>

                        <div class="col_half col_last tright nobottommargin">
                            <a href="#">Next Question &rArr;</a>
                        </div>

                    </div>

                    <div class="line"></div>--><!-- .post-navigation end -->

                    <!-- Post Author Info
                    ============================================= -->
                    <div class="card">
                        <div class="card-header text-left" style="background: none;"><strong>Posted by <a href="#">{{$Questions['first_name']}} {{$Questions['last_name']}}</a></strong></div>
                        <div class="card-body">
                            <div class="author-image">
                                <img src="https://onlinelms.skillsgroom.com/UI/teachers_profile_pic/{{$Questions['profile_pic']}}" alt="" class="rounded-circle">
                            </div>
                            <p>{!!$Questions['about_me']!!}</p>
                        </div>
                    </div><!-- Post Single - Author End -->

                    <!--<div class="line"></div>

                    <h4>Related Posts:</h4>

                    <div class="related-posts clearfix">

                        <div class="col_half nobottommargin">

                            <div class="mpost clearfix">
                                <div class="entry-image">
                                    <a href="#"><img src="images/forum/1.jpg" alt="Blog Single"></a>
                                </div>
                                <div class="entry-c">
                                    <div class="entry-title">
                                        <h4><a href="#">This is an Image Post</a></h4>
                                    </div>
                                    <ul class="entry-meta clearfix mb-1">
                                        <li><i class="icon-calendar3"></i> 10th July 2014</li>
                                        <li><a href="#"><i class="icon-comments"></i> 12</a></li>
                                    </ul>
                                    <div class="entry-content mt-0">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia nisi perferendis.</div>
                                </div>
                            </div>

                            <div class="mpost clearfix">
                                <div class="entry-image">
                                    <a href="#"><img src="images/forum/2.jpg" alt="Blog Single"></a>
                                </div>
                                <div class="entry-c">
                                    <div class="entry-title">
                                        <h4><a href="#">This is a Video Post</a></h4>
                                    </div>
                                    <ul class="entry-meta clearfix mb-1">
                                        <li><i class="icon-calendar3"></i> 24th July 2014</li>
                                        <li><a href="#"><i class="icon-comments"></i> 16</a></li>
                                    </ul>
                                    <div class="entry-content mt-0">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia nisi perferendis.</div>
                                </div>
                            </div>

                        </div>

                        <div class="col_half nobottommargin col_last">

                            <div class="mpost clearfix">
                                <div class="entry-image">
                                    <a href="#"><img src="images/forum/3.jpg" alt="Blog Single"></a>
                                </div>
                                <div class="entry-c">
                                    <div class="entry-title">
                                        <h4><a href="#">This is a Gallery Post</a></h4>
                                    </div>
                                    <ul class="entry-meta clearfix mb-1">
                                        <li><i class="icon-calendar3"></i> 8th Aug 2014</li>
                                        <li><a href="#"><i class="icon-comments"></i> 8</a></li>
                                    </ul>
                                    <div class="entry-content mt-0">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia nisi perferendis.</div>
                                </div>
                            </div>

                            <div class="mpost clearfix">
                                <div class="entry-image">
                                    <a href="#"><img src="images/forum/4.jpg" alt="Blog Single"></a>
                                </div>
                                <div class="entry-c">
                                    <div class="entry-title">
                                        <h4><a href="#">This is an Audio Post</a></h4>
                                    </div>
                                    <ul class="entry-meta clearfix mb-1">
                                        <li><i class="icon-calendar3"></i> 22nd Aug 2014</li>
                                        <li><a href="#"><i class="icon-comments"></i> 21</a></li>
                                    </ul>
                                    <div class="entry-content mt-0">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia nisi perferendis.</div>
                                </div>
                            </div>

                        </div>

                    </div>-->

                    <!-- Comments
                    ============================================= -->
                    <div id="comments" class="clearfix">

                        <h3 id="comments-title"><span>{{count($Comments)}}</span> Comments</h3>

                        <!-- Comments List
                        ============================================= -->
                        <ol class="commentlist clearfix">

                            @if($Comments)
                                @foreach($Comments as $Comment)
                                    <li class="comment even thread-even depth-1" id="li-comment-1">

                                        <div id="comment-1" class="comment-wrap clearfix">

                                            {{-- <div class="comment-meta">

                                                <div class="comment-author vcard">

                                                    <span class="comment-avatar clearfix">
                                                    <img alt="" src="images/forum/avatar.png" class="avatar avatar-60 photo avatar-default" height="60" width="60" /></span>

                                                </div>

                                            </div> --}}

                                            <div class="comment-content clearfix">

                                                <div class="comment-author">{{$Comment['name']}}<span><a href="#" title="Permalink to this comment">{{date('M d Y', strtotime($Comment['created_at']))}} at {{date('h:i a', strtotime($Comment['created_at']))}}</a></span></div>

                                                <p>{{$Comment['comment']}}</p>

                                                {{-- <a class="comment-reply-link" href="#"><i class="icon-reply"></i></a> --}}

                                            </div>

                                            <div class="clear"></div>

                                        </div>


                                    </li>
                                @endforeach
                            @else
                                <h3>No Comments Found...</h3>
                            @endif
                        </ol><!-- .commentlist end -->

                        <div class="clear"></div>

                        <!-- Comment Form
                        ============================================= -->
                        <div id="respond" class="clearfix">

                            <h3>Leave a <span>Comment</span></h3>

                            <div class="alert alert-success" id="ShowMsg" style="display: none;">
                                <strong id="SussMsg"></strong>
                              </div>


                            <form class="clearfix" action="#" method="post" id="commentform">

                                <div class="col_one_third">
                                    <label for="author">Name</label>
                                    <input type="hidden" name="author" id="question_id" value="{{$Questions['id']}}" size="22" tabindex="1" class="sm-form-control" />
                                    <input type="text" name="author" id="name" value="" size="22" tabindex="1" class="sm-form-control" />
                                </div>

                                <div class="col_one_third">
                                    <label for="email">Email</label>
                                    <input type="text" name="email" id="email" value="" size="22" tabindex="2" class="sm-form-control" />
                                </div>

                                <div class="col_one_third col_last">
                                    <label for="url">Location</label>
                                    <input type="text" name="url" id="location" value="" size="22" tabindex="3" class="sm-form-control" />
                                </div>

                                <div class="clear"></div>

                                <div class="col_full">
                                    <label for="comment">Comment</label>
                                    <textarea name="comment" cols="58" rows="7" id="comment" tabindex="4" class="sm-form-control"></textarea>
                                </div>

                                <div class="col_full nobottommargin">
                                    <a href="javascript:void(0);" id="submit_comment" class="button button-3d nomargin">Submit Comment</a>
                                </div>

                            </form>

                        </div><!-- #respond end -->

                    </div><!-- #comments end -->

                </div>


                    </div>

                </div>
            </div>
        </div>


        </div>
    </div>
</div>
@endsection


