<?php

namespace App\Http\Controllers\UI;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Admin\Courses;


class HomeController extends Controller
{
    public function home(){
        $title = "Home";

        $GetCourses= json_decode(file_get_contents("https://onlinelms.skillsgroom.com/api/course_list/2"), true);

        // echo json_encode($GetCourses['data']);
        // exit;

        $Courses = $GetCourses['data'];

        $GetTeachers = json_decode(file_get_contents("https://onlinelms.skillsgroom.com/api/teachers_list"), true);



        $Teachers = $GetTeachers['data'];

        // echo json_encode($GetTeachers['data']);
        // exit;
        // $Courses = Courses::select('courses.id', 'admin.id as TeacherId', 'courses.course_name', 'admin.name', 'courses.type','courses.fees', 'courses.featured_image')->join('admin','admin.id', '=', 'courses.teacher_id')->where('courses.status', 1)->orderBy('courses.created_at', 'DESC')->get();

        return view('UI.home', compact('Courses', 'Teachers'));
    }

    public function partners(){
        $title = "Home";

        return view('UI.partners');
    }

    public function book_demo($course_id, $id){
        $title = "Book Demo";

        $Courses = Courses::where('id', $course_id)->first();
        return view('UI.book_demo');
    }

    public function course_list(){
        $title = "Course List";

        $GetCourses= json_decode(file_get_contents("https://onlinelms.skillsgroom.com/api/course_list/2"), true);

        $Courses = $GetCourses['data'];

        return view('UI.course_list', compact('Courses'));
    }


    public function course_details($id){
        // $Teachers = Authendication::where('user_type', 2)->get();
        $GetCourses= json_decode(file_get_contents("https://onlinelms.skillsgroom.com/api/course_details/".$id), true);

        $Courses = $GetCourses['data'];

        // echo json_encode($Courses);
        // exit;

        // $Courses = Courses::where('id', $id)->first();
        return view('UI.course_details', compact('Courses'));

    }


    public function advisor_details($id){
        // $Teachers = Authendication::where('user_type', 2)->get();
        $GetTeachers = json_decode(file_get_contents("https://onlinelms.skillsgroom.com/api/teachers_details/".$id), true);

        $Teachers = $GetTeachers['data'];

        $GetTestimonials = json_decode(file_get_contents("https://onlinelms.skillsgroom.com/api/testimonials_details/".$id), true);

        $Testimonials = $GetTestimonials['data'];

        $GetCourses = json_decode(file_get_contents("https://onlinelms.skillsgroom.com/api/course_list_basedon_teachers/".$id), true);

        $Courses = $GetCourses['data'];

        $GetVideos = json_decode(file_get_contents("https://onlinelms.skillsgroom.com/api/videos_list/".$id), true);

        $Videos = $GetVideos['data'];

        // echo json_encode($Videos);
        // exit;

        // $Courses = Courses::where('id', $id)->first();
        return view('UI.advisor_details', compact('Teachers', 'Testimonials', 'Courses', 'Videos'));

    }


    public function search_course(Request $request){
        $title = "Home";

        $Name = $request->course_name;

        $GetCourses= json_decode(file_get_contents("https://onlinelms.skillsgroom.com/api/search_course/".$Name), true);


        $Courses = $GetCourses['data'];

        // $Courses = Courses::select('courses.id', 'admin.id as TeacherId', 'courses.course_name', 'admin.name', 'courses.type','courses.fees', 'courses.featured_image')->join('admin','admin.id', '=', 'courses.teacher_id')->where('courses.status', 1)->orderBy('courses.created_at', 'DESC')->get();

        return view('UI.search_courses', compact('Courses'));
    }

}
